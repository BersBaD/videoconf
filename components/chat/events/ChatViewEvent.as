package com.vci.components.chat.events
{
	import flash.events.Event;
	
	public class ChatViewEvent extends Event
	{
	/**	public static const SHOW_PARTICIPANTS:String='SHOW_PARTICIPANTS';
		public static const HIDE_PARTICIPANTS:String='HIDE_PARTICIPANTS';
		public static const SHOW_CHAT:String='SHOW_CHAT';
		public static const HIDE_CHAT:String='HIDE_CHAT';
		*/
		public static const TOGGLE_CHAT_VIEW:String='TOGGLE_CHAT_VIEW';
		public static const UPDATE_BTNS:String='UPDATE_BTNS';
		public function ChatViewEvent(type:String)
		{
			super(type);
		}
	}
}
