package com.vci.components.chat.events
{
	import flash.events.Event;
	
	public class UserlistToolbarEvent extends Event
	{
		public static const SORT_BY_TIME:String='sortByTime';
		public static const UN_SORT_BY_TIME:String='unSortByTime';
		public static const SORT_BY_RIGHTS:String='sortByRights';
		public static const UN_SORT_BY_RIGHTS:String='unSortByRights';

		public function UserlistToolbarEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
