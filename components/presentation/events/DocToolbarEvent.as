package com.vci.components.presentation.events
{
	import flash.events.Event;
	
	public class DocToolbarEvent extends Event
	{
		public static const CLICK_FILES:String='clickFiles';
		public static const CLICK_ZOOM_PLUS:String='clickZoomPlus';
		public static const CLICK_ZOOM_OUT:String='clickZoomMinus';
		public static const CLICK_FIT_WIDTH:String='clickFitWidth';
		public static const CLICK_FIT_HEIGHT:String='clickFitHeight';
		public static const CLICK_PAGE_PREV:String='clickPagePrev';
		public static const CLICK_PAGE_NEXT:String='clickPageNext';
		
		private var data:Object;
		public function DocToolbarEvent(type:String, data:Object=null)
		{
			this.data=data;
			super(type);
		}
	}
}
