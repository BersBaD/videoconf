package com.vci.components.calendar.events
{
	import com.vci.components.calendar.model.ICalendarConfVO;
	
	import flash.events.Event;
	
	public class CalendarEvent extends Event
	{
		public static const DAY_CLICK:String='day_click';
		public static const DAY_OVER:String='day_over';
		public static const SELECTED_CHANGED:String='selected_changed';
		public static const EDIT_CONFERENCE:String='editConference';
		public static const VIEW_CONFERENCE:String='viewConference';
		public static const DOWNLOAD_CONFERENCE:String='downloadConference';
		public static const DELETE_CONFERENCE:String='deleteConference';
		public static const SET_VIEW_BY_MONTH:String='setViewByMonth';
		
		private var data:Object;
		
		public function get dayNum():int {
			return data as int;
		}
		
		public function get calendarConfVO():ICalendarConfVO{
			return data as ICalendarConfVO;
		}
		
		public function CalendarEvent(type:String,data:Object=null){
			this.data=data;
			super(type);
		}
		
		override public function clone():Event{
			var ev:Event=new CalendarEvent(type,data);
			return ev;
		}
	}
}
