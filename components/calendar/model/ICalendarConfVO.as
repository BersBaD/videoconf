package com.vci.components.calendar.model
{
	/**
	 * Must be moved to 
	 */ 
	public interface ICalendarConfVO
	{
		function get name():String;
		function get beginDate():Date;
	}
}
