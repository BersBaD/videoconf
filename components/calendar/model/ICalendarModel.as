package com.vci.components.calendar.model
{
	public interface ICalendarModel
	{
		function getDaysInMonth(month:int, year:int):int;
		function getConfsInDay(day:int, month:int, year:int):int;
		/**
		 * Returns first week day of given month
		 */ 
		function getMonthFirstDay(month:int, year:int):int;
		
		function setSelectedDay(day:int):void;
		function setSelectedMonth(month:int):void;
		function setSelectedYear(year:int):void;
		
		function getSelectedDay():int;
		function getSelectedMonth():int;
		function getSelectedYear():int;
		
		function getMonthAsString(month:int):String;
		function getMonthGenAsString(month:int):String;
	}
}
