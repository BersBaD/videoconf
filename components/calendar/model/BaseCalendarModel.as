package com.vci.components.calendar.model
{
	import com.vci.components.calendar.events.CalendarEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.resources.ResourceManager;

	/**
	 * Base model for CalendarPanel ui component
	 */ 
	public class BaseCalendarModel implements ICalendarModel
	{
		private var _currentMonthNum:int=new Date().getMonth();
		private var _currentYear:int=new Date().getFullYear();
		private var _currentDay:int=new Date().getDate();
		
		//private var _currentDate=new Date();
		
		/**
		 * Used for retreival of localized month name from Resource
		 */ 
		private static const monthNames:Array=[
			"january",
			"february",
			"mart",
			"april",
			"may",
			"june",
			"july",
			"august",
			"september",
			"october",
			"november",
			"december"			
		]

		
		public var eventDispatcher:EventDispatcher;
		
		public function getDaysInMonth(month:int, year:int):int
		{
			return 0;
		}
		
		public function getConfsInDay(day:int, month:int, year:int):int
		{
			return 0;
		}	
		
		/**
		 * TODO: Use localized first day of week for non-russian locales
		 */ 
		public function getMonthFirstDay(month:int, year:int):int
		{
			var n:int=new Date(year,month,1).getDay();
			if (n==0){
				n=6; 
			} else {
				n--;
			}
			return n;		
		}
		
		public function setSelectedDay(day:int):void
		{
			if (_currentDay!=day){
				if (_currentDay<1){
					_currentMonthNum-=1;
					
					if (_currentMonthNum<0){
						_currentMonthNum=11;
						_currentYear-=1;
					} 
					day=getDaysInMonth(_currentMonthNum,_currentYear);
				} else {
					if (_currentDay>getDaysInMonth(_currentMonthNum,_currentYear)){
						_currentMonthNum+=1;
						if (_currentMonthNum>11){
							_currentMonthNum=0;
							_currentYear+=1;
						}
						day=1;						
					}
				}
				_currentDay=day;
				dispatchChange();
			}
		}
		
		public function setSelectedMonth(month:int):void
		{
			if (_currentMonthNum!=month){
				_currentMonthNum=month;
				
				if (_currentMonthNum<0){
					_currentMonthNum=11;
					_currentYear-=1;
				} else {
					if (_currentMonthNum>11){
						_currentMonthNum=0;
						_currentYear+=1;
					}
				}

				dispatchChange();
			}
		}
		
		public function setSelectedYear(year:int):void
		{
			if (_currentYear!=year){
				_currentYear=year;
				dispatchChange();
			}
		}
		
		public function getSelectedDay():int
		{
			return _currentDay;
		}
		
		public function getSelectedMonth():int
		{
			return _currentMonthNum;
		}
		
		public function getSelectedYear():int
		{
			return _currentYear;
		}
		
		public function getMonthAsString(month:int):String
		{
			return ResourceManager.getInstance().getString('ui_res', monthNames[month]);
		}
		
		public function getMonthGenAsString(month:int):String{
			return ResourceManager.getInstance().getString('ui_res', monthNames[month]+'Gen');
		}
		
		protected function dispatchChange():void{
			eventDispatcher.dispatchEvent(new CalendarEvent(CalendarEvent.SELECTED_CHANGED));
		}
	}
}
