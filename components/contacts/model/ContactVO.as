package com.vci.components.contacts.model
{
	public class ContactVO
	{
		public var num:int;
		public var name:String;
		public var email:String;
		public var company:String;
		public var title:String;
		public var selected:Boolean;
		
		public var rawData:Object;
	}
}
