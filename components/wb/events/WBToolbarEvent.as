package com.vci.components.wb.events
{
	import flash.events.Event;
	
	public class WBToolbarEvent extends Event
	{
		public static const SAVE:String='save';
		public static const PRINT:String='print';
		public static const CLEAR_ALL:String='clear_all';
		public static const SELECT_TOOL:String='select_tool';
		private var data:Object;
		public function get toolId():String{
			return data as String;
		}
		public function WBToolbarEvent(type:String, data:Object=null)
			
		{
			this.data=data;
			super(type, bubbles, cancelable);
		}
	}
}
