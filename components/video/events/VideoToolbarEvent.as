package com.vci.components.video.events
{
	import flash.events.Event;
	
	public class VideoToolbarEvent extends Event
	{
		public static const EXPAND	:String='expand';
		public static const COLLAPSE:String='collapse';
		public static const HIDE_FITTED:String='hideFitted';
		public function VideoToolbarEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
