package com.vci.components.home.events
{
	import flash.events.Event;

	public class StartMenuEvent extends Event
	{
		public static const QUICKSTART_CLICK:String='QUICKSTART_CLICK';
		public static const SCHEDULE_CLICK:String='SCHEDULE_CLICK';
		public static const PROFILE_CLICK:String='PROFILE_CLICK';
		public static const HELP_CLICK:String='HELP_CLICK';
		public function StartMenuEvent(type:String)
		{
			super(type);
		}
	}
}
