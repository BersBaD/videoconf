package com.vci.components.poll.events
{
	import flash.events.Event;
	
	public class PollToolbarEvent extends Event
	{
		/**
		 * Organizer: create new poll 
		 */
		public static const CLICK_POLL_CREATE:String='click_poll_create';

		/**
		 * Organizer: edit new poll 
		 */
		public static const CLICK_POLL_EDIT:String='click_poll_edit';
		
		/**
		 * Organizer: save (set) new poll
		 */ 
		public static const CLICK_POLL_SAVE:String='click_poll_save';
		
		/**
		 * Organizer: start poll 
		 */
		public static const CLICK_POLL_START:String='click_poll_start';
		
		/**
		 * Organizer: stop poll 
		 */
		public static const CLICK_POLL_STOP:String='click_poll_stop';
		
		
		
		public function PollToolbarEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
