package com.vci.conference.model.so
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.robotlegs.mvcs.Actor;
	
	import ru.pwi.logging.Log;
	import ru.pwi.net.so.ReliableSO;
	import ru.pwi.net.so.SOEvent;
	
	public class BasicSO extends Actor
	{
		protected var delay : uint = 3000;
		/**
		 * SO Name
		 */ 
		protected var name:String;
		protected var so : ReliableSO;
		protected var timer : Timer = null;
		protected var logger: Log;
		
		public function BasicSO(name:String)
		{
			logger=Log.getLog(this);
			this.name=name;
		}
		public function initSO(isRecord : Boolean) : void {
			try {
				logger.debug("[init]");
				if (so!=null){
					reset();
				}
				if (!isRecord) {
					startTimer();
				}
				so = new ReliableSO(name);
				so.addEventListener(SOEvent.CHANGE, soChanged);
				so.addEventListener(SOEvent.SUCCESS, soChanged);
				so.connect();
			} catch(e : Error) {
				logger.error("SharedObject Error: " + e.message);
			}
		}
		
		
		public function reset() : void {
			logger.debug("[reset]");
			deInitTimer();
			if (so != null) {
				so.removeEventListener(SOEvent.CHANGE, soChanged);
				so.removeEventListener(SOEvent.SUCCESS, soChanged);
				so.close();
				so = null;
			}
		}
		
		private function startTimer() : void {
			logger.debug("[startTimer]");
			deInitTimer();
			timer = new Timer(delay);
			timer.addEventListener(TimerEvent.TIMER, checkData);
			timer.start();
		}
		

		protected function checkData(event : TimerEvent) : void {
		}
		/**
		 * Implement in child all needed functinality on SO updates
		 */ 
		protected function soChanged(event : SOEvent) : void {
			DebugLogger.log(this,'HHHHHHHHHHHHHHHH');
		}
		
		private function deInitTimer() : void {
			logger.debug("[deInitTimer]");
			if (timer != null) {
				timer.stop();
				timer = null;
			}
		}
		
	}
}
