package com.vci.conference.model
{
	import com.vci.conference.components.confcontrol.model.ConferenceModel;
	
	import flash.events.IEventDispatcher;
	
	import org.robotlegs.mvcs.Actor;
	
	import ru.pwi.model.ConferenceManagerModel;
	import ru.pwi.model.ModelLocator;
	
	public class Model extends Actor implements IModel
	{
		public function get  modelLocator():Object{
			return ModelLocator.getInstance();
		}
		
		/**
		 * true if current user is owner of conference 
		 */ 
		public function get isConfOwner():Boolean{
			return ConferenceManagerModel.isOwner;
		}
		
		/**
		 * returns conference application event bus (singleton?)
		 */ 
		public function get eventBus():IEventDispatcher{
			return modelLocator.conference.events_bus;
		}
		
		public function get isRecord():Boolean{
			return modelLocator.isRecord;
		}
	}
}
