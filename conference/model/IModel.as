package com.vci.conference.model {
	import flash.events.IEventDispatcher;
	/**
 * @author vkostin
 */
	public interface IModel {
		function get modelLocator():Object;//:ModelLocator=ModelLocator.getInstance();
		
		/**
		 * true if current user is owner of conference 
		 */ 
		function get isConfOwner():Boolean;
		
		/**
		 * returns conference application event bus (singleton?)
		 */ 
		function get eventBus():IEventDispatcher;
		
		
	}
}
