package com.vci.conference.events
{
	import flash.events.Event;
	
	public class UIEvent extends Event
	{
		public function UIEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
