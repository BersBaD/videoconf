package com.vci.conference.events
{
	import flash.events.Event;
	
	public class AppEvent extends Event
	{
		/**
		 * Dispatchet in the preinit phase
		 */ 
		public static const APP_INIT:String='APP_INIT';
		
		/**
		 * Dipatched on Application onRegister is complete
		 */ 
		public static const APP_COMPLETE:String='APP_COMPLETE';
		/**
		 * Dipatched then GUI workflowstate changed
		 */ 
		public static const WORKFLOW_STATE_CHANGED:String='WORKFLOW_STATE_CHANGED';
		private var data:Object;
		public function get state():String{
			return data as String;
		}
		public function AppEvent(type:String,data:Object=null)
		{
			this.data=data;
			super(type);
		}
	}
}
