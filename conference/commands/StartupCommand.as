package com.vci.conference.commands
{
	import com.vci.conference.helpers.EventRouter;
	
	import mx.core.FlexGlobals;
	
	import org.robotlegs.mvcs.Command;
	
	import ru.pwi.logging.Log;
	import ru.pwi.model.ModelLocator;

	/**
	 * Called at the startup of an application.
	 */ 
	public class StartupCommand extends Command
	{
		
		override public function execute():void{
			
			var logger:Log=Log.getLog(this);
			logger.debug('StartupCommand#execute');

			
			/**
			 * Needed initialisation before main app ui class is initialized
			 */ 
			var modelLocator:ModelLocator=ModelLocator.getInstance();
			
		}
	}
}
