package com.vci.conference.commands
{
	import com.vci.conference.helpers.EventRouter;
	
	import org.robotlegs.mvcs.Command;
	
	public class StartupUICommand extends Command
	{
		[Inject]
		public var router:EventRouter;

		override public function execute():void{
			router.init();
		}
	}
}
