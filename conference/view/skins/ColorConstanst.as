package com.vci.conference.view.skins
{
	/**
	 * Common Colour constants - not to use styles 
	 */ 
	public class ColorConstanst
	{
		public static const ODD_COLOUR:uint=0xf1f1f1;
		public static const EVEN_COLOUR:uint=0xffffff;
	}
}
