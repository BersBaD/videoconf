package com.vci.conference.view {
	import mx.utils.ObjectUtil;
	import com.vci.conference.model.IModel;

	import org.robotlegs.mvcs.Mediator;

	import flash.events.Event;
	
	/**
	 * Common mediator for transition from current framework to robotlegs
	 */ 
	public class TransitionalMediator extends Mediator
	{
		public function cloneAndDispatch(ev:Event):void{
			var event:Event=ev.clone();
			//trace(ObjectUtil.toString(event));
			dispatch(event);
		}
	}
}
