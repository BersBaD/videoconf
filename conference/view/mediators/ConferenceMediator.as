package com.vci.conference.view.mediators
{
	import com.vci.components.chat.events.ChatViewEvent;
	import com.vci.conference.events.AppEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	import ru.pwi.helpers.UIViewHelper;
	import ru.pwi.logging.Log;
	import ru.pwi.model.ConferenceManagerModel;
	import ru.pwi.model.GUIState;
	import ru.pwi.model.ModelLocator;
	
	public class ConferenceMediator extends Mediator
	{
		[Inject]
		public var viewRef:Conference_1;
		
		private var logger:Log = Log.getLog(ConferenceMediator);
		private var modelLocator:ModelLocator=ModelLocator.getInstance();
		
		override public function onRegister():void{
			
			addContextListener(AppEvent.WORKFLOW_STATE_CHANGED, onWorkflowStateChanged);
			
			renderInitialView();
			
			addContextListener(ChatViewEvent.TOGGLE_CHAT_VIEW,onToggleChatView,ChatViewEvent);

			dispatch(new AppEvent(AppEvent.APP_COMPLETE));
		}
		
		private function onWorkflowStateChanged(ev:AppEvent):void{
			logger.info('#onWorkflowStateChanged '+ev.type);
			switch (ev.state){
				case GUIState.STATE_CONNECTED:
					onConnected();
					break;
				case GUIState.STATE_WAIT_CONFERENCE_CLOSE:
					onWaitClose();
					break;
			}
		}
		
		private function onConnected():void{
			viewRef.isShowRightPanel=true;
			modelLocator.chatIsVisible=true;
			modelLocator.pollIsEnabled=ConferenceManagerModel.isOwner;
//			viewRef.rightVBox1.visible=true;//ev.type==ChatViewEvent.SHOW_CHAT;
//			viewRef.rightVBox1.includeInLayout=true;//viewRef.rightVBox1.visible;
		}
		
		private function onWaitClose():void{
			viewRef.isShowRightPanel=false;
		}
		
/*		private function shParticipants(ev:ChatViewEvent):void{
			modelLocator.userListIsVisible=ev.type==ChatViewEvent.SHOW_PARTICIPANTS;
		}		
		private function shChat(ev:ChatViewEvent):void{
			viewRef.rightVBox0.visible=ev.type==ChatViewEvent.SHOW_PARTICIPANTS;
			viewRef.rightVBox0.includeInLayout=viewRef.rightVBox0.visible;
		}		
	*/	
		private function renderInitialView():void{
			viewRef.rightPanel.visible=false;
			viewRef.isShowRightPanel=false;
			modelLocator.chatIsVisible=false;
		}
		
		private function onToggleChatView(ev:ChatViewEvent):void{
			DebugLogger.log(this,'TOGGLE_CHAT_VIEW');
			if (modelLocator.guiState.conferenceIsActive){
				viewRef.isShowRightPanel=!viewRef.isShowRightPanel;
				if (viewRef.isShowRightPanel&&!modelLocator.userListIsVisible){
					modelLocator.chatIsVisible=true;
				}
			} else {
				viewRef.isShowRightPanel=false;
			}
		}
	}
}
