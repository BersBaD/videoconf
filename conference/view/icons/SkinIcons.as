package com.vci.conference.view.icons
{
	/**
	 * All icon images in one class
	 */ 
	public class SkinIcons
	{
		[Embed(source='/assets/com/vci/chat/icons/icDesktop.png')]
		[Bindable] public static var AVOnPrivIcon:Class;
		[Embed(source='/assets/com/vci/chat/icons/icDesktopOff.png')]
		[Bindable] public static var AVOffPrivIcon:Class;
		[Embed(source='/assets/com/vci/chat/icons/icCamera.png')]
		[Bindable] public static var CameraIcon:Class;
		[Embed(source='/assets/com/vci/chat/icons/icCameraOff.png')]
		[Bindable] public static var CameraIconCross:Class;
		
		[Embed(source='/assets/com/vci/chat/icons/icGreenFlag.png')]
		[Bindable] public static var GreenIcon:Class;
		[Embed(source='/assets/com/vci/chat/icons/icRedFlag.png')]
		[Bindable] public static var RedIcon:Class;
		[Embed(source='/assets/com/vci/chat/icons/icCross.png')]
		[Bindable] public static var DeleteIcon:Class;
		[Embed(source='/assets/com/vci/chat/icons/icMic.png')]
		[Bindable] public static var MicIcon:Class;
		[Embed(source='/assets/com/vci/chat/icons/icMicOff.png')]
		[Bindable] public static var MicIconCross:Class;
		
		[Embed(source='/assets/com/vci/chat/icons/icKeyboard.png')]
		[Bindable] public static var ModeratorOFF:Class;
		[Embed(source='/assets/com/vci/chat/icons/icKeyboardYes.png')]
		[Bindable] public static var ModeratorON:Class;
		
		[Embed(source='/assets/com/vci/chat/icons/icKeyboardNo.png')]
		[Bindable] public static var ModeratorONForGuest:Class;
		
		[Embed(source='/assets/com/vci/chat/icons/icBubble.png')]
		[Bindable] public static var BubbleIcon:Class;
	}
}
