package com.vci.conference.components.confcontrol.commands
{
	import com.vci.conference.components.confcontrol.events.PrivelegeChangeEvent;
	import com.vci.conference.model.Model;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Command;
	
	import ru.pwi.conference.broadcasting.command.MuteParticipantAudioCommand;
	import ru.pwi.conference.broadcasting.command.MuteParticipantVideoCommand;
	import ru.pwi.conference.privileges.PrivilegesVO;
	import ru.pwi.conference.privileges.command.SetUserPrivilegesCom;
	import ru.pwi.conference.user.model.UserVO;
	
	/***
	 * Changes user audio/video privileges according to event.type field if 
	 * @see com.vci.conference.components.confcontrol.events.PrivelegeChangeEvent
	 */ 
	public class PrivelegesChangeCommand extends Command
	{
		[Inject]
		public var model:Model;
		[Inject]
		public var event:PrivelegeChangeEvent;

		override public function execute():void{
			/**
			 * Change visual/audial 
			 */ 
			switch (event.type){
				case PrivelegeChangeEvent.ENABLE_AUDIO:
					new MuteParticipantAudioCommand(event.targetUserVO.userID, true).execute();
					break;
				case PrivelegeChangeEvent.DISABLE_AUDIO:
					new MuteParticipantAudioCommand(event.targetUserVO.userID, false).execute();
					break;
				case PrivelegeChangeEvent.ENABLE_VIDEO:
					new MuteParticipantVideoCommand(event.targetUserVO.userID, true).execute();
					break;
				case PrivelegeChangeEvent.DISABLE_VIDEO:
					DebugLogger.log(this,'PrivelegeChangeEvent.DISABLE_VIDEO');
					new MuteParticipantVideoCommand(event.targetUserVO.userID, false).execute();
					break;
				
			}

			/**
			 * If user is Owner of conference - change privileges of Broadcaster - is done in UserList component!
			
			if (model.isConfOwner){
				var vo:PrivilegesVO=event.targetUserVO.privileges;
				if (vo==null){
					//? Error?
					DebugLogger.error(this,' empty privileges in targetUserVO ');
				} 
				switch (event.type){
					case PrivelegeChangeEvent.ENABLE_AUDIO:
						vo.hasAudio=true;break;
					case PrivelegeChangeEvent.DISABLE_AUDIO:
						vo.hasAudio=false;break;
					case PrivelegeChangeEvent.ENABLE_VIDEO:
						vo.hasVideo=true;break;
					case PrivelegeChangeEvent.DISABLE_VIDEO:
						vo.hasVideo=false;break;
				}
				DebugLogger.log(this,'vo'+ObjectUtil.toString(vo));
				new SetUserPrivilegesCom(event.targetUserVO.userID, vo).execute();  
			}*/
		}
	}
}
