package com.vci.conference.components.confcontrol.events
{
	import flash.events.Event;
	
	public class ConferenceEvent extends Event
	{
		//Called when new conference started
		public static const START_NEW_CONFERENCE:String='START_NEW_CONFERENCE';
		public static const CLOSE_CONFERENCE:String='CLOSE_CONFERENCE';

		public function ConferenceEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
