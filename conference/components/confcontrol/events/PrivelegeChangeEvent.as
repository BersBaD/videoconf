package com.vci.conference.components.confcontrol.events
{
	import flash.events.Event;
	
	import ru.pwi.conference.user.model.UserVO;
	
	public class PrivelegeChangeEvent extends Event
	{
		public static const ENABLE_AUDIO:String='ENABLE_AUDIO';
		public static const DISABLE_AUDIO:String='DISABLE_AUDIO';
		public static const ENABLE_VIDEO:String='ENABLE_VIDEO';
		public static const DISABLE_VIDEO:String='DISABLE_VIDEO';
		
		public var targetUserVO:UserVO;
		
		public function PrivelegeChangeEvent(type:String,targetUserVO:UserVO)
		{
			this.targetUserVO=targetUserVO;
			super(type);
		}
		
		override public function clone():Event{
			var ev:PrivelegeChangeEvent=new PrivelegeChangeEvent(this.type,this.targetUserVO);
			return ev;
		}
	}
}
