package com.vci.conference.components.confcontrol.model
{
	import com.vci.conference.model.IModel;
	
	import org.robotlegs.mvcs.Actor;
	
	public class ConferenceModel extends Actor
	{
		[Inject]
		public var model:IModel;
		
		/**
		 * True if Conference is played as a record
		 */ 
		public function get isRecord():Boolean
		{
			return model.modelLocator.isRecord;
		}
	}
}
