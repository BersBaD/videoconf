package com.vci.conference.components.rightpanel.view.mediators
{
	import com.vci.components.chat.events.ChatViewEvent;
	import com.vci.components.chat.view.ChatTabBar;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.events.AppEvent;
	import com.vci.conference.model.Model;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.core.FlexGlobals;
	
	import org.robotlegs.mvcs.Mediator;
	
	import ru.pwi.conference.chat.view.Chat;
	import ru.pwi.model.ConferenceManagerModel;
	import ru.pwi.model.GUIState;
	import ru.pwi.model.ModelLocator;
	
	/**
	 * ChatTabBar is actually RightColumnTabbar
	 */ 
	public class ChatTabBarMediator extends Mediator
	{
		[Inject]
		public var viewRef:ChatTabBar;
		
		[Inject]
		public var model:Model;
		
		override public function onRegister():void{
			viewRef.currentState='expanded';
			viewRef.toggleParticipantsBtn.selected=true;
			viewRef.toggleParticipantsBtn.addEventListener(MouseEvent.CLICK,onClickParticipants);
			viewRef.toggleRightPanelBtn.addEventListener(MouseEvent.CLICK,onClickRightPanel);
			viewRef.toggleChatBtn.addEventListener(MouseEvent.CLICK,onClickChat);
			viewRef.togglePollBtn.addEventListener(MouseEvent.CLICK,onClickPollBtn);
			addContextListener(AppEvent.WORKFLOW_STATE_CHANGED, onWorkflowStateChanged);
			addContextListener(ChatViewEvent.UPDATE_BTNS,updateBtns);
		}
		
		/**
		 * Updates view
		 */ 
		private function updateBtns(ev:Event):void{
			viewRef.pollIsEnabled=model.modelLocator.pollIsEnabled && !model.isRecord;
			viewRef.togglePollBtn.selected=model.modelLocator.pollIsVisible;
			
			viewRef.toggleParticipantsBtn.selected=model.modelLocator.userListIsVisible;
			viewRef.toggleChatBtn.selected=model.modelLocator.chatIsVisible;
			if (viewRef.togglePollBtn.selected&&(viewRef.toggleChatBtn.selected||viewRef.toggleParticipantsBtn.selected)){
				viewRef.toggleChatBtn.selected=false;
				viewRef.toggleParticipantsBtn.selected=false;
				model.modelLocator.userListIsVisible=false;
				model.modelLocator.chatIsVisible=false;
			} 
			if (viewRef.toggleRightPanelBtn.selected){
				viewRef.currentState='collapsed';
//				viewRef.btnsHGroup.layout=viewRef.collapsedLayout;
			}else{
				viewRef.currentState='expanded';
//				viewRef.btnsHGroup.layout=viewRef.expandedLayout;
			}			
		}
		
		private function onWorkflowStateChanged(ev:AppEvent):void{
			switch (ev.state){
				case GUIState.STATE_CONNECTED:
					model.modelLocator.userListIsVisible=true;
					model.modelLocator.pollIsVisible=false;
					updateBtns(null);
					break;
			}
		}
		
		private function onClickChat(ev:MouseEvent):void{
			model.modelLocator.chatIsVisible=viewRef.toggleChatBtn.selected;
			if (model.modelLocator.chatIsVisible&&model.modelLocator.pollIsVisible){
				model.modelLocator.pollIsVisible=false;
			}
			updatBtnsByStates();
			checkAllInvisible();
		}
		
		private function onClickParticipants(ev:MouseEvent):void{
			model.modelLocator.userListIsVisible=viewRef.toggleParticipantsBtn.selected;
			if (model.modelLocator.userListIsVisible&&model.modelLocator.pollIsVisible){
				model.modelLocator.pollIsVisible=false;
			}
			updatBtnsByStates();
			checkAllInvisible();
		}
		
		private function onClickPollBtn(ev:MouseEvent):void{
			model.modelLocator.pollIsVisible=viewRef.togglePollBtn.selected;
			settleStates();
			updatBtnsByStates();
			checkAllInvisible();
			if (model.modelLocator.pollIsVisible){
				dispatch(new PollEvent(PollEvent.SHOW_POLL_COMPONENT));
			}
		}

		private function onClickRightPanel(ev:MouseEvent):void{
			dispatch(new ChatViewEvent(ChatViewEvent.TOGGLE_CHAT_VIEW));
			dispatch(new ChatViewEvent(ChatViewEvent.UPDATE_BTNS));
		}
		
		private function checkAllInvisible():void{
			if (!(model.modelLocator.chatIsVisible||model.modelLocator.userListIsVisible||model.modelLocator.pollIsVisible)){
				viewRef.toggleRightPanelBtn.selected=true;
				dispatch(new ChatViewEvent(ChatViewEvent.TOGGLE_CHAT_VIEW));
				dispatch(new ChatViewEvent(ChatViewEvent.UPDATE_BTNS));
			}
		}
		
		private function settleStates():void{
			if (model.modelLocator.pollIsVisible){
				model.modelLocator.chatIsVisible=false;
				model.modelLocator.userListIsVisible=false;
			}
		}
		
		private function updatBtnsByStates():void{
			viewRef.toggleParticipantsBtn.selected=model.modelLocator.userListIsVisible;
			viewRef.toggleChatBtn.selected=model.modelLocator.chatIsVisible;
			viewRef.togglePollBtn.selected=model.modelLocator.pollIsVisible;
		}
	}
}
