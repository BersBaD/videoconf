package com.vci.conference.components.broadcaster.view.mediators {
	import ru.pwi.conference.broadcasting.view.BroadcasterView;
	import ru.pwi.conference.user.event.PrivilegesChangedEvent;

	import com.vci.conference.model.IModel;
	import com.vci.conference.view.TransitionalMediator;

	import mx.utils.ObjectUtil;
	
	public class BroadcasterViewMediator extends TransitionalMediator
	{
		[Inject]
		public var viewRef:BroadcasterView;
		[Inject]
		public var model:IModel;		
		
		override public function onRegister():void{
			eventMap.mapListener(model.eventBus, PrivilegesChangedEvent.PRIVILEGES_CHANGED, onPrivilegesChanged,PrivilegesChangedEvent);
		}
		override public function onRemove():void{
			eventMap.unmapListener(model.eventBus, PrivilegesChangedEvent.PRIVILEGES_CHANGED, onPrivilegesChanged,PrivilegesChangedEvent);
		}
		
		/***
		 * Event handlers 
		 */
		 private function onPrivilegesChanged(ev:PrivilegesChangedEvent):void{
			DebugLogger.log(this,ObjectUtil.toString(ev));
			if (ev.userID==viewRef.userID){
				viewRef.btnMicEnabled=ev.privileges.hasAudio;
				viewRef.btnCameraEnabled=ev.privileges.hasVideo;
   		    }
		 } 		
	}
}
