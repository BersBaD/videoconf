package com.vci.conference.components.broadcaster.view.mediators
{
	import com.vci.conference.components.confcontrol.commands.PrivelegesChangeCommand;
	import com.vci.conference.components.confcontrol.events.PrivelegeChangeEvent;
	import com.vci.conference.model.Model;
	import com.vci.conference.view.TransitionalMediator;
	
	import org.robotlegs.mvcs.Mediator;
	
	import ru.pwi.conference.broadcasting.view.ParticipantView;
	import ru.pwi.conference.user.event.PrivilegesChangedEvent;
	
	public class ParticipantViewMediator extends TransitionalMediator
	{
		[Inject]
		public var viewRef:ParticipantView;
		
		override public function onRegister():void{
			addViewListener(PrivelegeChangeEvent.DISABLE_AUDIO,cloneAndDispatch,PrivelegeChangeEvent);
			addViewListener(PrivelegeChangeEvent.DISABLE_VIDEO,cloneAndDispatch,PrivelegeChangeEvent);
			addViewListener(PrivelegeChangeEvent.ENABLE_AUDIO,cloneAndDispatch,PrivelegeChangeEvent);
			addViewListener(PrivelegeChangeEvent.ENABLE_VIDEO,cloneAndDispatch,PrivelegeChangeEvent);
			
			//eventMap.mapListener(model.eventBus, PrivilegesChangedEvent.PRIVILEGES_CHANGED, onPrivilegesChanged,PrivilegesChangedEvent);
		}
		override public function onRemove():void{
			eventMap.unmapListener(viewRef,PrivelegeChangeEvent.DISABLE_AUDIO,cloneAndDispatch);
			eventMap.unmapListener(viewRef,PrivelegeChangeEvent.DISABLE_VIDEO,cloneAndDispatch);
			eventMap.unmapListener(viewRef,PrivelegeChangeEvent.ENABLE_AUDIO,cloneAndDispatch);
			eventMap.unmapListener(viewRef,PrivelegeChangeEvent.ENABLE_VIDEO,cloneAndDispatch);
			
			//eventMap.unmapListener(model.eventBus, PrivilegesChangedEvent.PRIVILEGES_CHANGED, onPrivilegesChanged,PrivilegesChangedEvent);
		}
		
		/***
		 * Event handlers 
		 
		private function onPrivilegesChanged(ev:PrivilegesChangedEvent):void{
			if (ev.userID==viewRef.userID){
				if (ev.privileges.hasVideo){
					
				} else {
					viewRef.removeVideo();
				}
			}
		}*/ 
	}
}
