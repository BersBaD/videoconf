package com.vci.conference.components.poll.services {
	import com.vci.conference.components.poll.model.vo.PollVO;
	import com.vci.conference.components.poll.model.vo.PollVotesList;
/**
 * @author vkostin
 */
	public interface IPollService {
		function confSetPoll(vo:PollVO):void;
		
		//function confDelPoll();

		function confVotePoll(vo:PollVotesList):void;
		
		function confClosePoll():void;
		
		function confShowPoll(isVisible:Boolean):void;
	}
}
