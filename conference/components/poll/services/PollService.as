package com.vci.conference.components.poll.services {
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.vo.PollVO;
	import com.vci.conference.components.poll.model.vo.PollVoteVO;
	import com.vci.conference.components.poll.model.vo.PollVotesList;
	
	import flash.events.TimerEvent;
	import flash.net.Responder;
	import flash.utils.Timer;
	
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Actor;
	
	import ru.pwi.logging.Log;
	import ru.pwi.net.remoteCall.RemoteDelegate;
	import ru.pwi.net.so.ReliableSO;
	import ru.pwi.net.so.SOEvent;

	/**
	 * Вызов удаленных процедур
	 */
	public class PollService extends Actor implements IPollService {
		
		private var so : ReliableSO;
		private var timer : Timer = null;
		private var logger : Log = Log.getLog(PollService);

		public function confSetPoll(vo : PollVO) : void {
			var responder : Responder = new Responder(defaultSuccess, defaultFail);
			var rd : RemoteDelegate = new RemoteDelegate(responder);
			rd.confSetPoll(vo);
		}

		public function confVotePoll(vo : PollVotesList) : void {
			var responder : Responder = new Responder(defaultSuccess, defaultFail);
			var rd:RemoteDelegate=new RemoteDelegate(responder);
			rd.confVotePoll(vo);
		}

		public function confClosePoll() : void {
			var responder : Responder = new Responder(defaultSuccess, defaultFail);
			var rd:RemoteDelegate=new RemoteDelegate(responder);
			rd.confClosePoll();
		}
		
		public function confShowPoll(isVisible:Boolean):void{
			var responder : Responder = new Responder(defaultSuccess, defaultFail);
			var rd:RemoteDelegate=new RemoteDelegate(responder);
			rd.confShowPoll(isVisible);
		}
		
		private function defaultSuccess(info:Object) : void {
			logger.debug('[defaultSuccess] ' + ObjectUtil.toString(info));
		}
		
		private function defaultFail(info:Object) : void {
			logger.debug('[defaultFail] ' + ObjectUtil.toString(info));
		}
	}
}
