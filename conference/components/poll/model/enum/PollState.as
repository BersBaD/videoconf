package com.vci.conference.components.poll.model.enum
{
	/**
	 * Poll Component states
	 */ 
	public class PollState
	{
		public static const NEW_STATE:String='new';
		public static const EDITING_STATE:String='editing';
		public static const STARTED_STATE:String='started';
		public static const ANSWERING_STATE:String='answering';
		public static const FINISHED_STATE : String = 'finished';
		public static const SUBMITTING_EDITED : String = "submittingEdited";
		public static const SUBMITTING_ANSWERS : String = "submittingAnswers";
		public static const NONE : String = "none";
	}
}
