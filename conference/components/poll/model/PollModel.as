package com.vci.conference.components.poll.model {
	import com.vci.components.chat.events.ChatViewEvent;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.vo.*;
	import com.vci.conference.model.IModel;
	import mx.resources.*;
	import org.robotlegs.mvcs.Actor;
	import ru.pwi.logging.Log;

	public class PollModel extends Actor {
		[Inject]
		public var model:IModel;

		public static const MAX_ANSWERS : int = 7;
		public static const MAX_QUESTIONS : int = 3;

		public var pollVO : PollVO;
		public var pollResults : PollResultsVO;
		public var pollIsFinished : Boolean;

		private var logger : Log = Log.getLog(PollModel);
		private var resourceManager:IResourceManager=ResourceManager.getInstance();

		public function get pollItems() : Vector.<PollQuestionVO> {
			return pollVO.questions;
		}

		public function get pollTitle() : String {
			return pollVO.title;
		}

		public function set pollTitle(title : String) : void {
			pollVO.title = title;
		}

		public function PollModel() : void {
			clear();
		}

		public function get numQuestions() : int {
			return pollItems.length;
		}

		public function clear() : void {
			pollVO = new PollVO();
			// pollItems=new Vector.<PollQuestionVO>();
		}

		private var _currentState : String = 'none';

		public function set currentState(state : String) : void {
			if (state != _currentState) {
				_currentState = state;
				DebugLogger.log(this, 'POLL: new state set '+state);
				dispatch(new PollEvent(PollEvent.STATE_CHANGED));
			}
		}

		public function get currentState() : String {
			return _currentState;
		}

		public function addNewQuestion() : PollQuestionVO {
			var vo : PollQuestionVO = new PollQuestionVO();
			vo.answers.push(new PollAnswerVO());
			//vo.answers.push(new PollAnswerVO());
			vo.answers[0].answerNumber=0;
			//vo.answers[1].answerNumber=1;
			vo.answers[0].answerName="";//resourceManager.getString('text', 'poll_enter_a')+'1';
			//vo.answers[1].answerName=resourceManager.getString('text', 'poll_enter_a')+'2';
			vo.question=resourceManager.getString('text', 'poll_enter_q_hint');
			pollItems.push(vo);
			return vo;
		}

		public function getQuestion(i : int) : PollQuestionVO {
			return pollItems[i];
		}
		
		public function set isEnabled(v:Boolean):void{
			model.modelLocator.pollIsEnabled=v;	
		}
		
		public function get isEnabled():Boolean{
			return Boolean(model.modelLocator.pollIsEnabled); 
		}
		
		public function set isVisible(v:Boolean):void{
			model.modelLocator.pollIsEnabled=v;
			model.modelLocator.pollIsVisible=v;
			dispatch (new ChatViewEvent(ChatViewEvent.UPDATE_BTNS));
		}
		
		public function get isVisible():Boolean{
			return Boolean(model.modelLocator.pollIsVisible); 
		}
		
		private var _resultsImmidiatlyVisible:Boolean=false;
		
		public function set resultsImmidiatlyVisible(val:Boolean):void{
			_resultsImmidiatlyVisible=val;
			dispatch(new PollEvent(PollEvent.IMMIDIATE_RESULTS_CHANGED));
		}
		
		public function get resultsImmidiatlyVisible():Boolean{
			return _resultsImmidiatlyVisible;
		}
		
		public function setResultsImmidiatlyVisible(val:Boolean):void{
			_resultsImmidiatlyVisible=val;
		}
		
		public function convertToString():String
		{
			var fileContent:String = "";
			return fileContent;
		}

		public function get isMoreThenOneAnswer():Boolean{
			return pollVO && pollVO.questions && pollVO.questions.every(
					function(qItem:PollQuestionVO,...nah):Boolean{ return qItem && qItem.answers.length > 1; }
				);
		}
	}
}
