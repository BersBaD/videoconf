package com.vci.conference.components.poll.model.reporter
{
	public class AnswerArrayObject implements IPollResultAnswer
	{
		public var title:String;
		public var text:String;
		
		public function AnswerArrayObject()
		{
		}
		
		public function set resultsText(text:String):void
		{
			this.text = text;
		}
		
		public function get resultsText():String
		{
			return text;
		}
	}
}
