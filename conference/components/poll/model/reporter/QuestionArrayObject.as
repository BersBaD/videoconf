package com.vci.conference.components.poll.model.reporter
{
	public class QuestionArrayObject implements IPollResultRow
	{
		public var num:String;
		public var text:String;
		public var answers:Vector.<IPollResultAnswer> = new Vector.<IPollResultAnswer>();
		
		public function QuestionArrayObject()
		{
		}
		
		public function getAnswer(index:int):IPollResultAnswer
		{
			return answers[index];
		}
		
		public function addAnswer(answerText:String, resultsText:String):IPollResultAnswer
		{
			var answer:AnswerArrayObject = new AnswerArrayObject();
			answer.title = answerText;
			answer.text = resultsText;
			answers.push(answer);
			return answer;
		}
		
		public function get answersCount():int
		{
			return answers.length;
		}
	}
}
