package com.vci.conference.components.poll.model.reporter
{
	public interface IPollResultAnswer
	{
		function set resultsText(text:String):void;
		function get resultsText():String;
	}
}
