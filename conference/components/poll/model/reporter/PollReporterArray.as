package com.vci.conference.components.poll.model.reporter
{
	import com.vci.conference.components.poll.model.PollModel;
	
	public class PollReporterArray extends PollReporter
	{
		private var questions:Vector.<QuestionArrayObject>;
		
		public function PollReporterArray(model:PollModel, questionsArray:Vector.<QuestionArrayObject>)
		{
			super(model);
			questions = questionsArray;
		}
		
		override protected function addElement(questionNum:String, isOdd:Boolean, questionText:String):IPollResultRow
		{
			var q:QuestionArrayObject = new QuestionArrayObject();
			q.num = questionNum;
			q.text = questionText;
			questions.push(q);
			return q;
		}
		
		override protected function getPollResultRow(index:int):IPollResultRow
		{
			return questions[index];
		}
	}
}
