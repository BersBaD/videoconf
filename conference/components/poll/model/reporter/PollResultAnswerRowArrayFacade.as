package com.vci.conference.components.poll.model.reporter
{
	public class PollResultAnswerRowArrayFacade implements IPollResultAnswer
	{
		private var answer:AnswerArrayObject; 
		
		public function PollResultAnswerRowArrayFacade(answerObject:AnswerArrayObject)
		{
			answer = answerObject;
		}
		
		public function set resultsText(text:String):void
		{
			answer.text = text;
		}
		
		public function get resultsText():String
		{
			return answer.text;
		}
	}
}
