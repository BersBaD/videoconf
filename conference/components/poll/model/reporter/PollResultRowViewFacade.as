package com.vci.conference.components.poll.model.reporter
{
	import com.vci.components.poll.view.PollResultAnswerRow;
	import com.vci.components.poll.view.PollResultRow;

	public class PollResultRowViewFacade implements IPollResultRow
	{
		private var row:PollResultRow;
		
		public function PollResultRowViewFacade(rowView:PollResultRow)
		{
			row = rowView;
		}
		
		public function addAnswer(answerText:String, resultsText:String):IPollResultAnswer
		{
			var pollAnswer : PollResultAnswerRow = new PollResultAnswerRow();
			pollAnswer.answer.text = answerText;
			pollAnswer.results.text = resultsText;
			row.answers.addElement(pollAnswer);
			return new PollResultAnswerRowViewFacade(pollAnswer)
		}
		
		public function getAnswer(index:int):IPollResultAnswer
		{
			return new PollResultAnswerRowViewFacade(row.answers.getElementAt(index) as PollResultAnswerRow);
		}
		
		public function get answersCount():int
		{
			return row.answers.numElements;
		}
	}
}
