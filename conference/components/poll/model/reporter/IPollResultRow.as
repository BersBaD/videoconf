package com.vci.conference.components.poll.model.reporter
{
	public interface IPollResultRow
	{
		function getAnswer(index:int):IPollResultAnswer;
		function addAnswer(answerText:String, resultsText:String):IPollResultAnswer;
		function get answersCount():int;
	}
}
