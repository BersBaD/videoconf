package com.vci.conference.components.poll.model.reporter
{
	import com.vci.components.poll.view.PollResultAnswerRow;

	public class PollResultAnswerRowViewFacade implements IPollResultAnswer
	{
		private var row:PollResultAnswerRow;
		
		public function PollResultAnswerRowViewFacade(rowView:PollResultAnswerRow)
		{
			row = rowView;
		}
		
		public function set resultsText(text:String):void
		{
			row.results.text = text;
		}
		
		public function get resultsText():String
		{
			return row.results.text;
		}
	}
}
