package com.vci.conference.components.poll.model.reporter
{
	import com.vci.components.poll.view.PollResultRow;
	import com.vci.components.poll.view.PollResultsView;
	import com.vci.conference.components.poll.model.PollModel;
	
	import mx.states.OverrideBase;

	public class PollReporterView extends PollReporter
	{
		private var viewRef:PollResultsView;
		
		public function PollReporterView(model:PollModel, view:PollResultsView)
		{
			super(model);
			viewRef = view;
		}
		
		override protected function addElement(questionNum:String, isOdd:Boolean, questionText:String):IPollResultRow
		{
			var pollResultElem:PollResultRow = new PollResultRow();
			pollResultElem.questionNum.text = questionNum;
			pollResultElem.isOdd = isOdd;
			pollResultElem.question.text = questionText;
			viewRef.pollElements.addElement(pollResultElem);
			return new PollResultRowViewFacade(pollResultElem)
		}
		
		override protected function getPollResultRow(index:int):IPollResultRow
		{
			return new PollResultRowViewFacade(viewRef.pollElements.getElementAt(index) as PollResultRow);
		}
	}
}
