package com.vci.conference.components.poll.model.reporter
{
	import com.vci.components.poll.view.PollResultAnswerRow;
	import com.vci.components.poll.view.PollResultRow;
	import com.vci.components.poll.view.PollResultsView;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.vo.PollResultVO;
	
	import mx.utils.ObjectUtil;

	public class PollReporter
	{
		private var pollModel:PollModel;
		
		public function PollReporter(model:PollModel)
		{
			pollModel = model;
		}
		
		public function createQuestionsAnswers() : void {
			DebugLogger.log(this,ObjectUtil.toString( pollModel.pollItems));
			var l : int = pollModel.numQuestions;
			for (var i : int = 0;i < l;i++) {
				var pollResultElem:IPollResultRow = addElement('' + (i+1), (i & 1) == 0, pollModel.pollItems[i].question);
				var a_l : int = pollModel.pollItems[i].answers.length;
				for (var j : int = 0;j < a_l;j++) {
					pollResultElem.addAnswer(pollModel.pollItems[i].answers[j].answerName, ' 0% \\ 0');
				}
			}
		}
		
		public function updateAnswers():void
		{
			if (pollModel.pollResults==null){
				DebugLogger.log(this,'UPDATE ANSWERS WITH NO POLL DATA!');
			} else {
				DebugLogger.log(this,'updateAnswers '+ObjectUtil.toString(pollModel.pollResults));
				var l : int = pollModel.pollResults.results.length;
				var vo : PollResultVO;
				var totals : Array = new Array();
				
				var i : int;
				for (i = 0;i < pollModel.numQuestions;i++) {
					totals.push(0);
				}
				
				for (i = 0;i < l;i++) {
					vo = pollModel.pollResults.results[i];
					totals[vo.pollQuestionNumber] += vo.votesNumber;
				}
				var pollResultElem : IPollResultRow;
				var pollResultAnswer : IPollResultAnswer;
				for (i = 0;i < pollModel.numQuestions;i++) {
					pollResultElem = getPollResultRow(i); 
					l=pollModel.pollItems[i].answers.length;
					for (var j:int=0;j<l;j++){
						pollResultAnswer = pollResultElem.getAnswer(j);
						pollResultAnswer.resultsText= '0% \\ 0';
					}
				}
				
				l = pollModel.pollResults.results.length;
				for (i = 0;i < l;i++) {
					DebugLogger.log(this,'questionNumber='+i);
					vo = pollModel.pollResults.results[i];
					pollResultElem = getPollResultRow(vo.pollQuestionNumber);
					if (pollResultElem == null) {
						DebugLogger.log(this,'error!');
					} else {
						//trace('vo.pollAnswerNumber=' + vo.pollAnswerNumber);
						DebugLogger.log(this,'vo.pollAnswerNumber=' + vo.pollAnswerNumber+' '+Math.round(vo.votesNumber / totals[vo.pollQuestionNumber] * 100) );
						if ((!isNaN(vo.pollAnswerNumber)) && pollResultElem.answersCount > vo.pollAnswerNumber && vo.pollAnswerNumber >= 0) {
							pollResultAnswer = pollResultElem.getAnswer(vo.pollAnswerNumber);
							if (pollResultAnswer != null) {
								var total : int = totals[vo.pollQuestionNumber];
								if (total<=0){
									DebugLogger.error(this,'total<=0 ');
								} else {
									DebugLogger.log(this,' pollResultAnswer.results.text='+Math.round(vo.votesNumber / total * 100) + ' \\ ' + vo.votesNumber );
									pollResultAnswer.resultsText = Math.round(vo.votesNumber / total * 100) + ' \\ '+ vo.votesNumber ;
								}
							} else {
								DebugLogger.log(this,'error in created view, vo='+ObjectUtil.toString(vo));
							}
						} else {
							DebugLogger.log(this,'error in input data, vo='+ObjectUtil.toString(vo));
						}
					}
				}
			}
		}
		
		protected function addElement(questionNum:String, isOdd:Boolean, questionText:String):IPollResultRow
		{
			throw new Error("Must be overrided");
		}
		
		protected function getPollResultRow(index:int):IPollResultRow
		{
			throw new Error("Must be overrided");
		}
	}
}
