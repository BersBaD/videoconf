package com.vci.conference.components.poll.model.vo {
	import mx.utils.ObjectUtil;

	/**
 	* @author vkostin
 	*/
	public class PollVO {
		public var title : String;
		public var isVisible : Boolean;
		public var questions : Vector.<PollQuestionVO>;

		public function PollVO() : void {
			questions = new Vector.<PollQuestionVO>();
		}

		public static function fromAMFObject(obj : Object) : PollVO {
			//trace('fromAMFObject' + ObjectUtil.toString(obj));
			var result : PollVO = new PollVO();
			result.title = obj.pollName;
			if (!(obj.pollQuestions is Array)) {
				obj.pollQuestions = [obj.pollQuestions];
			}
			var arr : Array = obj.pollQuestions as Array;
			if (arr != null) {
				for (var i : int = 0;i < arr.length;i++) {
					var questionVO : PollQuestionVO = new PollQuestionVO();
					var questionObj : Object = arr[i];
					if (questionObj != null) {
						if (!(questionObj.questionAnswers is Array)) {
							questionObj.questionAnswers = [questionObj.questionAnswers];
						}
						var answers : Array = questionObj.questionAnswers  as Array;
						if (answers != null) {
							for (var j : int = 0;j < answers.length;j++) {
								if (answers[j] != null) {
									var vo : PollAnswerVO = new PollAnswerVO();
									vo.answerName = answers[j].answerName;
									vo.answerNumber = answers[j].answerNumber;
									questionVO.answers.push(vo);
								}
							}
							questionVO.question = questionObj.questionName;
						}
						result.questions.push(questionVO);
					}
				}
			} else{
				DebugLogger.log(result,'obj.pollQuestions is not Array!');
			}
			return result;
		}
	}
}
