package com.vci.conference.components.poll.model.vo {
	/**
 * @author vkostin
 */
	public class PollAnswerVO {
		public var answerNumber:int;
		public var answerName:String;
	}
}
