package com.vci.conference.components.poll.model.vo {
/**
 * @author vkostin
 */
	public class PollVoteVO {
		public var votesNumber:int;
		public var pollQuestionNumber:int;
		public var pollAnswerNumber:int;
	}
}
