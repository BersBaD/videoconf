package com.vci.conference.components.poll.model.vo {
/**
 * @author vkostin
 */
	public class PollResultsVO {
		public var results:Vector.<PollResultVO>;
		public function PollResultsVO():void{
			results=new Vector.<PollResultVO>();
		}
	}
}
