package com.vci.conference.components.poll.model.vo
{
	public class PollQuestionVO
	{
		public var question:String;
		public var answers:Vector.<PollAnswerVO>;
		public var selectedItem:int;
		
		public function PollQuestionVO():void{
			answers=new Vector.<PollAnswerVO>();
			selectedItem=-1;
		}
	}
}
