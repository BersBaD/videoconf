package com.vci.conference.components.poll.model.so
{
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.model.so.BasicSO;
	
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Actor;
	
	import ru.pwi.net.so.SOEvent;
	
	public class VoteObjectSO extends BasicSO
	{
		private static const VOTE_PROPERTY_NAME : String = "voteProperty";
		
		public function VoteObjectSO()
		{
			super('voteObject');
		}
		override protected function soChanged(event : SOEvent) : void {
			logger.debug('soChanged event.propertyName='+event.propertyName+' value='+ObjectUtil.toString(so.getProperty(event.propertyName)));
			switch (event.propertyName) {
				case VOTE_PROPERTY_NAME:
					dispatch(new PollEvent(PollEvent.NEW_VOTE_DATA_IS_SET, so.getProperty(event.propertyName)));
					break;
			}			
		}		
	}
}
