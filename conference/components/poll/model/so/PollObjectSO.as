package com.vci.conference.components.poll.model.so
{
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.model.so.BasicSO;
	
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Actor;
	
	import ru.pwi.net.so.SOEvent;
	
	public class PollObjectSO extends BasicSO
	{
		private static const POLL_PROPERTY_NAME : String = "pollProperty";
		private static const VISIBLE_PROPERTY_NAME:String="isVisibleProperty";
		private static const CLOSE_PROPERTY_NAME : String = "closeProperty";
		
		public function PollObjectSO()
		{
			super('pollObject');
		}
		override protected function soChanged(event : SOEvent) : void {
			logger.debug('soChanged event.propertyName='+event.propertyName+' value='+ObjectUtil.toString(so.getProperty(event.propertyName)));
			switch (event.propertyName) {
				case POLL_PROPERTY_NAME:
					dispatch(new PollEvent(PollEvent.NEW_POLL_IS_SET, so.getProperty(event.propertyName)));
					break;
				case CLOSE_PROPERTY_NAME:
					dispatch(new PollEvent(PollEvent.POLL_CLOSED_DATA_IS_SET, so.getProperty(event.propertyName)));
					break;
				case VISIBLE_PROPERTY_NAME:
					dispatch(new PollEvent(PollEvent.RESULTS_VISIBLE_CHANGED,so.getProperty(event.propertyName)));
					break;
			}			
		}
	}
}
