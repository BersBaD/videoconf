package com.vci.conference.components.poll.events {
	import com.vci.conference.components.poll.model.vo.PollResultsVO;
	import com.vci.conference.components.poll.model.vo.PollVotesList;
	
	import flash.events.Event;
	
	public class PollEvent extends Event
	{
		/**
		 * On changed Poll component state 
		 */ 
		public static const STATE_CHANGED : String = 'poll_state_changed';
		/**
		 * Is called then poll set is successfull
		 */ 
		public static const POLL_SET_SUCCESS : String = "POLL_SET_SUCCESS";
		/**
		 * Is called then poll set failed
		 */ 
		public static const POLL_SET_FAILED : String = "POLL_SET_FAILED";
		/**
		 * Is called then so data changed
		 */ 
		public static const DATA_CHANGED : String = "DATA_CHANGED";
		/**
		 * Is called then new poll is set
		 */ 
		public static const NEW_POLL_IS_SET : String = "NEW_POLL_IS_SET";
		/**
		 * Is called then new vote data is set
		 */ 
		public static const NEW_VOTE_DATA_IS_SET : String = "NEW_VOTE_DATA_IS_SET";
		/**
		 * Is called then poll is closed
		 */ 
		public static const POLL_CLOSED_DATA_IS_SET : String = "POLL_CLOSED_DATA_IS_SET";
		/**
		 * Is called then submitting vote. Parameter = PollVotesList
		 */ 
		public static const SUBMIT_VOTE : String = "SUBMIT_VOTE";
		/**
		 * Is called then Poll is finished. Parameter - PollResultsVO
		 */ 
		public static const POLL_FINISHED : String = "POLL_FINISHED";
		
		public static const SHOW_POLL_COMPONENT:String='SHOW_POLL_COMPONENT';
		public static const UPDATE_VOTE_STATS:String="UPDATE_VOTE_STATS";
		
		/**
		 * Immidiate results option is changed by owner
		 */ 
		public static const IMMIDIATE_RESULTS_CHANGED:String="IMMIDIATE_RESULTS_CHANGED";
		
		public static const RESULTS_VISIBLE_CHANGED:String="RESULTS_VISIBLE_CHANGED";
		
		public var data : Object;
		//POLL_FINISHED
		public function get pollResults (): PollResultsVO{
			return data as PollResultsVO;
		}
		
		//SUBMIT_VOTE
		public function get pollVotes() : PollVotesList{
			return data as PollVotesList;
		}
		
		public function PollEvent(type:String, data:Object=null)
		{
			this.data=data;
			super(type, false, true);
		}
	}
}
