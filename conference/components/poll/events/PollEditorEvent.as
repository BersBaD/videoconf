package com.vci.conference.components.poll.events {
	import com.vci.conference.components.poll.model.vo.PollQuestionVO;
	import flash.events.Event;

	/**
	 * Poll editor event
	 * @author vkostin
	 */
	public class PollEditorEvent extends Event {
		/**
		 *  Poll Editor add answer
		 */ 
		public static const ADD_ANSWER : String = "ADD_ANSWER";
		/**
		 *  Poll Editor remove answer
		 */ 
		public static const REMOVE_ANSWER : String = "REMOVE_ANSWER";
		
		public var vo : PollQuestionVO;
		private var data : Object;
		
		public function get answerNum():int{
			return data as int;
		}
		
		public function PollEditorEvent(type : String, vo:PollQuestionVO=null, data:Object=null) {
			this.vo=vo;
			this.data=data;
			super(type, bubbles, cancelable);
		}
	}
}
