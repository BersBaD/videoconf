package com.vci.conference.components.poll.view.mediators {
	import com.vci.components.poll.view.PollResultAnswerRow;
	import com.vci.components.poll.view.PollResultRow;
	import com.vci.components.poll.view.PollResultsView;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.components.poll.model.reporter.AnswerArrayObject;
	import com.vci.conference.components.poll.model.reporter.IPollResultAnswer;
	import com.vci.conference.components.poll.model.reporter.IPollResultRow;
	import com.vci.conference.components.poll.model.reporter.PollReporter;
	import com.vci.conference.components.poll.model.reporter.PollReporterArray;
	import com.vci.conference.components.poll.model.reporter.PollReporterView;
	import com.vci.conference.components.poll.model.reporter.QuestionArrayObject;
	import com.vci.conference.components.poll.model.vo.PollQuestionVO;
	import com.vci.conference.components.poll.model.vo.PollResultVO;
	import com.vci.conference.components.poll.model.vo.PollResultsVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileReference;
	
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Mediator;

	/**
 	* @author vkostin
 	*/
	public class PollResultsViewMediator extends Mediator {
		[Inject]
		public var pollModel : PollModel;
		[Inject]
		public var viewRef : PollResultsView;
		private var reporter:PollReporterView;

		override public function onRegister() : void {
			addContextListener(PollEvent.STATE_CHANGED, onStateChanged);
			addContextListener(PollEvent.UPDATE_VOTE_STATS,updateVoteStats);
			eventMap.mapListener(viewRef.saveToFile, MouseEvent.CLICK, onSavePoll);
			reporter = new PollReporterView(pollModel, viewRef);
		}

		override public function onRemove() : void {
			eventMap.unmapListener(eventDispatcher, PollEvent.STATE_CHANGED, onStateChanged);
		}
		
		private function updateVoteStats(ev:PollEvent):void{
			DebugLogger.log(this,'updateVoteStats '+viewRef.pollElements.numElements);
			if (viewRef.pollElements.numElements<=0){
				//If not initiated - create view elements
				update();
			} else {
				updateAnswers();
			}
		}

		private function onStateChanged(ev : PollEvent) : void {
			switch (pollModel.currentState) {
				case PollState.STARTED_STATE:
				case PollState.FINISHED_STATE:
					update();
					break;
			}
		}

		private function update() : void {
			clearView();
			createQuestionsAnswers();
			viewRef.title.text=pollModel.pollTitle;
			if (pollModel.pollResults!=null){
				updateAnswers();
			}
		}

		private function updateAnswers() : void {
			reporter.updateAnswers();
		}

		private function createQuestionsAnswers() : void {
			reporter.createQuestionsAnswers();
		}

		private function clearView() : void {
			viewRef.pollElements.removeAllElements();
		}
		
		private function onSavePoll(ev:MouseEvent):void
		{
			var result:Vector.<QuestionArrayObject> = new Vector.<QuestionArrayObject>();
			var arrayReporter:PollReporterArray = new PollReporterArray(pollModel, result);
			arrayReporter.createQuestionsAnswers();
			arrayReporter.updateAnswers();
			var resultString:String = pollModel.pollTitle + "\r\n";
			for each (var row:QuestionArrayObject in result)
			{
				resultString += "\r\n" + row.num + ". " + row.text + "\r\n" 
				for each (var answer:AnswerArrayObject in row.answers)
				{
					resultString += answer.title + " - " + answer.text + "\r\n";
				}
			}
			var fr:FileReference = new FileReference();
			fr.save(resultString, "results.txt");
		}
	}
}
