package com.vci.conference.components.poll.view.mediators {
	import com.vci.components.poll.view.PollAnswerEditor;
	import com.vci.components.poll.view.PollQuestionEditor;
	import com.vci.conference.components.poll.events.PollEditorEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.vo.PollAnswerVO;
	import com.vci.conference.components.poll.model.vo.PollQuestionVO;
	
	import flash.events.Event;
	
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Mediator;

	public class PollQuestionEditorMediator extends Mediator {
		[Inject]
		public var viewRef : PollQuestionEditor;
		
		private var resourceManager:IResourceManager=ResourceManager.getInstance();
		
		private function get vo() : PollQuestionVO {
			return PollQuestionVO(viewRef.vo);
		}

		override public function onRegister() : void {
			addContextListener(PollEditorEvent.ADD_ANSWER, onAddAnswer);
			addContextListener(PollEditorEvent.REMOVE_ANSWER, onRemoveAnswer);
			eventMap.mapListener(viewRef.ti, Event.CHANGE, onChangeText);
			update();
		}

		private function onChangeText(ev:Event) : void {
			//trace(ObjectUtil.toString(vo));
			vo.question=viewRef.ti.text;
		}

		override public function onRemove() : void {
			
			eventMap.unmapListener(eventDispatcher, PollEditorEvent.ADD_ANSWER, onAddAnswer);
			eventMap.unmapListener(eventDispatcher, PollEditorEvent.REMOVE_ANSWER, onRemoveAnswer);
			eventMap.unmapListener(viewRef.ti, Event.CHANGE, onChangeText);
		}

		private function onRemoveAnswer(ev : PollEditorEvent) : void {
			if (ev.vo == viewRef.vo) {
				vo.answers.splice(ev.answerNum, 1);
				update();
			}
		}

		private function onAddAnswer(ev : PollEditorEvent) : void {
			if (ev.vo == viewRef.vo) {
				if (vo.answers.length < PollModel.MAX_ANSWERS) {
					var answerVO:PollAnswerVO=new PollAnswerVO();
					//answerVO.answerName="";//resourceManager.getString('text', 'poll_enter_a')+(vo.answers.length+1);
					answerVO.answerNumber=vo.answers.length;
					vo.answers.push(answerVO);
					addNewAnswerUI(vo.answers.length - 1);
				}
			}
		}

		private function addNewAnswerUI(num : int) : void {
			var element : PollAnswerEditor = new PollAnswerEditor();
			element.question = vo;
			element.answerNum = num;
			element.hasDel = num > 1;
			element.hasAdd = (vo.answers.length < PollModel.MAX_ANSWERS) && (num == vo.answers.length - 1);
			element.ti.text = vo.answers[num].answerName;
			viewRef.answers.addElement(element);
		}

		private function update() : void {
			//trace(ObjectUtil.toString(vo));
			viewRef.ti.text = vo.question;
			viewRef.answers.removeAllElements();
			for (var i : int = 0;i < vo.answers.length;i++) {
				// element.te
				addNewAnswerUI(i);
			}
		}
	}
}
