package com.vci.conference.components.poll.view.mediators {
	import com.vci.components.poll.events.PollToolbarEvent;
	import com.vci.components.poll.view.PollToolbar;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.view.TransitionalMediator;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class PollToolbarMediator extends TransitionalMediator
	{
		[Inject]
		public var viewRef:PollToolbar;
		[Inject]
		public var pollModel:PollModel;
		
		override public function onRegister():void {
			viewRef.currentState=pollModel.currentState;
			//trace('pollModel.currentState='+pollModel.currentState);
			//trace('viewRef.currentState='+viewRef.currentState);
			
			addViewListener(PollToolbarEvent.CLICK_POLL_CREATE, cloneAndDispatch);
			addViewListener(PollToolbarEvent.CLICK_POLL_START, cloneAndDispatch);
			addViewListener(PollToolbarEvent.CLICK_POLL_STOP, cloneAndDispatch);
			
			addContextListener(PollEvent.STATE_CHANGED, onPollStateChange);
			
			viewRef.cbImmidiate.addEventListener(MouseEvent.CLICK,onCBImmidiateClick);
			
			addContextListener(PollEvent.RESULTS_VISIBLE_CHANGED,onResultsVisibleChanged);
		}
		
		private function onPollStateChange(ev:PollEvent):void{
			viewRef.currentState=pollModel.currentState;
		}
		
		private function onCBImmidiateClick(ev:MouseEvent):void{
			pollModel.resultsImmidiatlyVisible=viewRef.cbImmidiate.selected;
		}
		
		private function onResultsVisibleChanged(ev:PollEvent):void{
			viewRef.cbImmidiate.selected=pollModel.resultsImmidiatlyVisible;
		}
	}
}
