package com.vci.conference.components.poll.view.mediators {
	import com.vci.components.poll.view.*;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.components.poll.model.vo.*;
	
	import flash.events.MouseEvent;
	import org.robotlegs.mvcs.Mediator;
	import spark.components.RadioButton;

	/**
	 * @author vkostin
	 */
	public class PollQuizViewMediator extends Mediator {
		[Inject]
		public var viewRef : PollQuizView;
		[Inject]
		public var pollModel : PollModel;

		override public function onRegister() : void {
			addContextListener(PollEvent.STATE_CHANGED, onStateChanged);
			eventMap.mapListener(viewRef.clearBtn, MouseEvent.CLICK, onClearClick);
			eventMap.mapListener(viewRef.submitBtn, MouseEvent.CLICK, onSubmitClick);
		}
		
		override public function onRemove() : void {
			eventMap.unmapListener(eventDispatcher, PollEvent.STATE_CHANGED, onStateChanged);
			eventMap.unmapListener(viewRef.clearBtn, MouseEvent.CLICK, onClearClick);
			eventMap.unmapListener(viewRef.submitBtn, MouseEvent.CLICK, onSubmitClick);
		}		

		private function clearView() : void {
			viewRef.pollElements.removeAllElements();
		}

		private function update() : void {
			clearView();
			viewRef.title.text = pollModel.pollTitle;
			for (var i : int = 0;i < pollModel.numQuestions;i++) {
				var questionVO : PollQuestionVO = pollModel.getQuestion(i);
				var element : PollElement = new PollElement();
				element.questionNum.text=''+(1+i);
				element.question.text = questionVO.question;
				for (var j : int = 0;j < questionVO.answers.length;j++) {
					var answerVO : PollAnswerVO = questionVO.answers[j];
					var answElement : RadioButton = new RadioButton();
					answElement.group = element.answersRBGrp;
					answElement.label = answerVO.answerName;
					element.answers.addElement(answElement);
				}
				viewRef.pollElements.addElement(element);
			}
		}

		private function onSubmitClick(ev:MouseEvent) : void {
			var pollResults : PollVotesList = new PollVotesList();
			for (var i : int = 0;i < pollModel.numQuestions;i++) {
				var questionVO : PollQuestionVO = pollModel.getQuestion(i);
				var element : PollElement = viewRef.pollElements.getElementAt(i) as PollElement;
				var vote : PollVoteVO = new PollVoteVO();
				var flag : Boolean=false;
				for (var j : int = 0;j < questionVO.answers.length;j++) {
					var answerRB : RadioButton = element.answers.getElementAt(j) as RadioButton;
					var answerVO : PollAnswerVO = questionVO.answers[j];
					if (answerRB.selected) {
						vote.pollQuestionNumber = i;
						vote.pollAnswerNumber = answerVO.answerNumber;
						flag =true;
						break;
					}
				}
				if (!flag){
					//TODO:'must select in all answers to submit
					return;
				}
				pollResults.votes.push(vote);
			}
			dispatch(new PollEvent(PollEvent.SUBMIT_VOTE,pollResults));
		}

		private function onClearClick(ev : MouseEvent) : void {
			update();
		}

		private function onStateChanged(ev : PollEvent) : void {
			switch(pollModel.currentState) {
				case PollState.ANSWERING_STATE:
					update();
					break;
			}
		}
	}
}

