package com.vci.conference.components.poll.view.mediators {
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.components.poll.view.PollView;
	import org.robotlegs.mvcs.Mediator;

	/**
 * @author vkostin
 */
	public class PollViewMediator extends Mediator {
		[Inject]
		public var viewRef : PollView;
		[Inject]
		public var model:PollModel;

		override public function onRegister() : void {
			addContextListener(PollEvent.STATE_CHANGED, onStateChanged);
		}

		private function onStateChanged(ev:PollEvent) : void {
			//trace('changing state '+model.currentState);
			viewRef.currentState=model.currentState;
		}

	}
}
