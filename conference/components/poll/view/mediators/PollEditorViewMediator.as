package com.vci.conference.components.poll.view.mediators {
	import com.vci.components.poll.view.PollEditorView;
	import com.vci.components.poll.view.PollQuestionEditor;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.components.poll.model.vo.PollQuestionVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import org.robotlegs.mvcs.Mediator;

	public class PollEditorViewMediator extends Mediator {
		[Inject]
		public var viewRef : PollEditorView;
		[Inject]
		public var pollModel : PollModel;
		
		private var resourceManager:IResourceManager=ResourceManager.getInstance();

		override public function onRegister() : void {
			addContextListener(PollEvent.STATE_CHANGED, onStateChanged);
			
			eventMap.mapListener(viewRef.addItemBtn, MouseEvent.CLICK, onAddItemClick);
			eventMap.mapListener(viewRef.clearBtn, MouseEvent.CLICK, onClearClick);
			eventMap.mapListener(viewRef.pollTitle, Event.CHANGE,onChangeQuizTitle);
		}

		private function onChangeQuizTitle() : void {
			pollModel.pollTitle=viewRef.pollTitle.text;
		}

		private function onClearClick(ev:MouseEvent) : void {
			pollModel.clear();
			pollModel.addNewQuestion();
			updateView();
		}

		private function onStateChanged(ev : PollEvent) : void {
			switch (pollModel.currentState) {
				case PollState.EDITING_STATE: {
					updateView();
				}
			}
		}

		/**
		 * methods (in purist's view should be moved to commands)
		 */
		private function addNewItem() : void {
			var elem : PollQuestionEditor = new PollQuestionEditor();
			var vo : PollQuestionVO = pollModel.addNewQuestion();
			elem.vo=vo;
			viewRef.pollElements.addElement(elem);
		}

		/**
		 * event handlers
		 */
		private function onAddItemClick(ev : MouseEvent) : void {
			addNewItem();
			updateAddItemBtn();
		}

		private function updateAddItemBtn() : void {
			viewRef.addItemBtn.enabled=(pollModel.numQuestions < PollModel.MAX_QUESTIONS);
		}

		private function updateView() : void {
			clearView();
			for (var i : int = 0;i < pollModel.numQuestions;i++) {
				var vo : PollQuestionVO = pollModel.getQuestion(i);
				var element : PollQuestionEditor = new PollQuestionEditor();
				element.vo = vo;
				viewRef.pollElements.addElement(element);
			}
			updateAddItemBtn();
			viewRef.pollTitle.text=pollModel.pollTitle;
		}

		private function clearView() : void {
			viewRef.pollElements.removeAllElements();
			updateAddItemBtn();
		}
	}
}
