package com.vci.conference.components.poll.view.mediators {
	import com.vci.conference.components.poll.model.vo.PollQuestionVO;

	import flash.events.Event;

	import com.vci.components.poll.view.PollAnswerEditor;
	import com.vci.conference.components.poll.events.PollEditorEvent;

	import org.robotlegs.mvcs.Mediator;

	import flash.events.MouseEvent;

/**
 * @author vkostin
 */
	public class PollAnswerEditorMediator extends Mediator {
		[Inject]
		public var viewRef : PollAnswerEditor;

		override public function onRegister() : void {
			eventMap.mapListener(viewRef.ti, Event.CHANGE, onChangeText);
			eventMap.mapListener(viewRef.addNewAnswerBtn, MouseEvent.CLICK, onClickAddAnswer);
			eventMap.mapListener(viewRef.removeAnswerBtn, MouseEvent.CLICK, onClickRemoveAnswer);
		}

		private function onChangeText(ev : Event) : void {
			var vo : PollQuestionVO = PollQuestionVO(viewRef.question);
			vo.answers[viewRef.answerNum].answerName = viewRef.ti.text;
		}

		override public function onRemove() : void {
			eventMap.unmapListener(viewRef.addNewAnswerBtn, MouseEvent.CLICK, onClickAddAnswer);
			eventMap.unmapListener(viewRef.removeAnswerBtn, MouseEvent.CLICK, onClickRemoveAnswer);
		}

		private function onClickRemoveAnswer(ev : MouseEvent) : void {
			dispatch(new PollEditorEvent(PollEditorEvent.REMOVE_ANSWER, PollQuestionVO(viewRef.question), viewRef.answerNum));
		}

		private function onClickAddAnswer(ev : MouseEvent) : void {
			viewRef.hasAdd = false;
			dispatch(new PollEditorEvent(PollEditorEvent.ADD_ANSWER, PollQuestionVO(viewRef.question), viewRef.answerNum));
		}
	}
}
