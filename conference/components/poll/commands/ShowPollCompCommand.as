package com.vci.conference.components.poll.commands
{
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	
	import org.robotlegs.mvcs.Command;
	
	import ru.pwi.model.ConferenceManagerModel;
	
	public class ShowPollCompCommand extends Command
	{
		[Inject]
		public var pollModel:PollModel;
		
		override public function execute():void
		{
			if (ConferenceManagerModel.isOwner){
				if (pollModel.currentState==PollState.NONE){
					pollModel.currentState=PollState.NEW_STATE;
				}
			} else {
				pollModel.isVisible=pollModel.isEnabled;
			}
		}
	}
}
