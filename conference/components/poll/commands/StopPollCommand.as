package com.vci.conference.components.poll.commands
{
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.components.poll.services.PollService;
	import com.vci.conference.model.IModel;
	
	import org.robotlegs.mvcs.Command;
	
	public class StopPollCommand extends Command
	{
		[Inject]
		public var service : PollService;
		[Inject]
		public var model : IModel;
		[Inject]
		public var pollModel:PollModel;
		
		override public function execute() : void {
			//trace('!model.isConfOwner');
			DebugLogger.log(this,'execute');
			
			if (model.isConfOwner) {
				service.confClosePoll();
			}
			pollModel.currentState=PollState.FINISHED_STATE;
		}
	}
}
