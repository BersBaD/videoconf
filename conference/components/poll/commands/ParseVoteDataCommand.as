package com.vci.conference.components.poll.commands
{
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.vo.PollResultVO;
	import com.vci.conference.components.poll.model.vo.PollResultsVO;
	import com.vci.conference.model.IModel;
	
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Command;
	
	/**
	 * Parses poll voting data and creates event to update view  
	 */ 
	public class ParseVoteDataCommand extends Command
	{
		[Inject]
		public var model:IModel;
		[Inject]
		public var pollModel:PollModel;
		[Inject]
		public var event:PollEvent;
		
		override public function execute():void
		{
			var pollResults:PollResultsVO=new  PollResultsVO();
			var votes:Array=event.data as Array;
			if (votes!=null){
				for (var i:int=0;i<votes.length;i++){
					var vote:Object=votes[i];
					var resultVO:PollResultVO=new PollResultVO();
					resultVO.pollAnswerNumber=vote.answerNumber as int;
					resultVO.pollQuestionNumber=vote.questionNumber as int;
					resultVO.votesNumber=vote.votesNumber as int;
					if (!isNaN(resultVO.pollAnswerNumber) && !isNaN(resultVO.pollQuestionNumber) && !isNaN(resultVO.votesNumber)){
						pollResults.results.push(resultVO);
					} else {
						DebugLogger.error(this,' wrong values in results, idx='+i+' data='+ObjectUtil.toString(vote));
					}
				}
				pollModel.pollResults=pollResults;
				dispatch(new PollEvent(PollEvent.UPDATE_VOTE_STATS));
			} else {
				DebugLogger.error(this,' null as a property value');
			}
		}
	}
}
