package com.vci.conference.components.poll.commands
{
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.services.IPollService;
	import com.vci.conference.model.Model;
	
	import org.robotlegs.mvcs.Command;
	
	public class PollSettingChangedCommand extends Command
	{
		[Inject]
		public var model:Model;
		[Inject]
		public var pollModel:PollModel;
		[Inject]
		public var service:IPollService;
		override public function execute():void
		{
			if (model.isConfOwner){
				service.confShowPoll(pollModel.resultsImmidiatlyVisible);
			}	
		}
	}
}
