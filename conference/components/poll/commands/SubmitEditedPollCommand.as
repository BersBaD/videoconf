package com.vci.conference.components.poll.commands {
import com.vci.conference.components.poll.model.PollModel;
import com.vci.conference.components.poll.model.enum.PollState;
import com.vci.conference.components.poll.services.IPollService;
import org.robotlegs.mvcs.Command;
import ru.pwi.helpers.AlertDefault;

public class SubmitEditedPollCommand extends Command {
	[Inject]
	public var service:IPollService;
	
	[Inject]
	public var model:PollModel;
	
	override public function execute() : void {
		if(model.isMoreThenOneAnswer){
			model.currentState=PollState.SUBMITTING_EDITED;
			service.confSetPoll(model.pollVO);
		}else{
			AlertDefault.show(L.ui_res.s('poll_more_then_one_alert'), L.ui_res.s('error'));
		}
	}
	
}}

