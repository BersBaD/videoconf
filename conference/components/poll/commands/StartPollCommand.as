package com.vci.conference.components.poll.commands {
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.components.poll.model.vo.PollVO;
	import com.vci.conference.model.IModel;
	
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Command;

	/**
	 * Called then poll data is set (changed) at server SO, or when conference is resumed after disconnect
	 * @author vkostin
	 */
	public class StartPollCommand extends Command {
		
		[Inject]
		public var pollModel : PollModel;
		
		[Inject]
		public var model : IModel;
		
		[Inject]
		public var event : PollEvent;

		override public function execute() : void {
			//trace('!model.isConfOwner');
//			DebugLogger.log(this,'event.data='+ObjectUtil.toString(event.data));
			pollModel.pollVO=PollVO.fromAMFObject(event.data);
			pollModel.pollResults=null;
			pollModel.pollIsFinished=false;
			if (model.isConfOwner) {
				pollModel.currentState=PollState.STARTED_STATE;
			} else {
				pollModel.currentState=PollState.ANSWERING_STATE;
				pollModel.isEnabled=true;
				dispatch(new PollEvent(PollEvent.SHOW_POLL_COMPONENT));
			}
		}
	}
}
