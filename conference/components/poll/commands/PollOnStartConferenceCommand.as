package com.vci.conference.components.poll.commands
{
	import com.vci.conference.components.confcontrol.model.ConferenceModel;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.components.poll.model.so.PollObjectSO;
	import com.vci.conference.components.poll.model.so.VoteObjectSO;
	import com.vci.conference.components.poll.services.IPollService;
	import com.vci.conference.components.poll.services.PollService;
	
	import org.robotlegs.mvcs.Command;
	
	/**
	 * Executed when new Conference or Record of conference starts
	 */ 
	public class PollOnStartConferenceCommand extends Command
	{
		[Inject]
		public var voteSO:VoteObjectSO;
	
		[Inject]
		public var pollSO:PollObjectSO;
		
		[Inject]
		public var pollModel:PollModel;
		
		[Inject]
		public var conferenceModel:ConferenceModel;
		
		override public function execute():void
		{
			pollModel.currentState=PollState.NONE;	
			pollSO.initSO(conferenceModel.isRecord);
			voteSO.initSO(conferenceModel.isRecord);
		}
	}
}
