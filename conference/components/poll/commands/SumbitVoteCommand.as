package com.vci.conference.components.poll.commands {
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.components.poll.services.IPollService;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author vkostin
	 */
	public class SumbitVoteCommand extends Command {
		[Inject]
		public var pollModel:PollModel;
		[Inject]
		public var event:PollEvent;
		[Inject]
		public var service:IPollService;
		
		override public function execute() : void {
			service.confVotePoll(event.pollVotes);
			if (pollModel.resultsImmidiatlyVisible){
				pollModel.currentState=PollState.FINISHED_STATE;
				
			} else {
				pollModel.currentState=PollState.SUBMITTING_ANSWERS;

			}
		}
	}
}
