package com.vci.conference.components.poll.commands
{
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	
	import org.robotlegs.mvcs.Command;
	
	public class ClosePollCommand extends Command
	{
		[Inject]
		public var pollModel:PollModel;
		override public function execute():void
		{
			pollModel.currentState=PollState.FINISHED_STATE;
		}
	}
}
