package com.vci.conference.components.poll.commands {
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import org.robotlegs.mvcs.Command;

	public class CreateNewPollCommand extends Command {
		[Inject]
		public var pollModel:PollModel;
		private var rm:IResourceManager=ResourceManager.getInstance();
		override public function execute() : void {
			//trace('CreateNewPollCommand ');
			pollModel.clear();
			var d:Date=new Date();
			pollModel.pollTitle=rm.getString('text', 'poll_autotitle')+' '+d.date+'.'+d.month+'.'+d.fullYear;
			
			pollModel.addNewQuestion();
			pollModel.currentState=PollState.EDITING_STATE;
		}
		
	}
}
