package com.vci.conference.components.poll.commands {
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author vkostin
	 */
	public class PollFinishedCommand extends Command {
 
		[Inject]
		public var pollModel:PollModel;
		[Inject]
		public var event:PollEvent;
		
		override public function execute() : void {
			pollModel.pollIsFinished=true;
			pollModel.pollResults=event.pollResults;
			pollModel.currentState=PollState.FINISHED_STATE;
		}
	}
}
