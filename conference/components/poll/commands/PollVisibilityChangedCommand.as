package com.vci.conference.components.poll.commands
{
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.enum.PollState;
	import com.vci.conference.model.Model;
	
	import org.robotlegs.mvcs.Command;
	
	public class PollVisibilityChangedCommand extends Command
	{
		[Inject]
		public var model:Model;
		
		[Inject]
		public var pollModel:PollModel;
		
		[Inject]
		public var event:PollEvent;
		
		override public function execute():void
		{
			DebugLogger.log(this,'execute');
			pollModel.setResultsImmidiatlyVisible(event.data);
			if (!model.isConfOwner&&!pollModel.pollIsFinished){
				DebugLogger.log(this,'!model.isConfOwner&&!pollModel.pollIsFinished pollModel.currentState='+pollModel.currentState);
				switch(pollModel.currentState){
					case PollState.SUBMITTING_ANSWERS:
						DebugLogger.log(this,'PollState.SUBMITTING_ANSWERS'+pollModel.resultsImmidiatlyVisible);
						if (pollModel.resultsImmidiatlyVisible){
							pollModel.currentState=PollState.FINISHED_STATE;
						}
						break;
					case PollState.FINISHED_STATE:
						DebugLogger.log(this,'PollState.FINISHED_STATE'+pollModel.resultsImmidiatlyVisible);
						if (!pollModel.resultsImmidiatlyVisible){
							pollModel.currentState=PollState.SUBMITTING_ANSWERS;
						}
						
				}
			}
		}
	}
}
