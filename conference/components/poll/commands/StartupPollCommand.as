package com.vci.conference.components.poll.commands {
	import com.vci.components.poll.events.PollToolbarEvent;
	import com.vci.components.poll.view.*;
	import com.vci.conference.components.confcontrol.events.ConferenceEvent;
	import com.vci.conference.components.poll.events.PollEvent;
	import com.vci.conference.components.poll.model.PollModel;
	import com.vci.conference.components.poll.model.so.PollObjectSO;
	import com.vci.conference.components.poll.model.so.VoteObjectSO;
	import com.vci.conference.components.poll.services.IPollService;
	import com.vci.conference.components.poll.services.PollService;
	import com.vci.conference.components.poll.view.mediators.*;
	import org.robotlegs.mvcs.Command;
	import ru.pwi.archive.calendar.model.Conference;

	/**
	 * Starts up commands, model, service and mediators of Poll component
	 * @author vkostin
	 */
	public class StartupPollCommand extends Command {
		
		override public function execute() : void {
			commandMap.mapEvent(PollToolbarEvent.CLICK_POLL_CREATE, CreateNewPollCommand);
			commandMap.mapEvent(PollToolbarEvent.CLICK_POLL_START, SubmitEditedPollCommand);
			commandMap.mapEvent(PollToolbarEvent.CLICK_POLL_STOP, StopPollCommand);

			commandMap.mapEvent(PollEvent.SHOW_POLL_COMPONENT,ShowPollCompCommand);
			commandMap.mapEvent(PollEvent.NEW_POLL_IS_SET,StartPollCommand);
			commandMap.mapEvent(PollEvent.SUBMIT_VOTE,SumbitVoteCommand);
			commandMap.mapEvent(PollEvent.POLL_FINISHED,PollFinishedCommand);
			
			commandMap.mapEvent(PollEvent.NEW_VOTE_DATA_IS_SET,ParseVoteDataCommand);
			commandMap.mapEvent(PollEvent.POLL_CLOSED_DATA_IS_SET,ParseVoteDataCommand);
			commandMap.mapEvent(PollEvent.POLL_CLOSED_DATA_IS_SET,ClosePollCommand);
			
			commandMap.mapEvent(ConferenceEvent.START_NEW_CONFERENCE, PollOnStartConferenceCommand);
			
			commandMap.mapEvent(PollEvent.IMMIDIATE_RESULTS_CHANGED,PollSettingChangedCommand);
			commandMap.mapEvent(PollEvent.RESULTS_VISIBLE_CHANGED,PollVisibilityChangedCommand);
			
			injector.mapSingleton(PollModel);
			
			injector.mapSingleton(VoteObjectSO);
			injector.mapSingleton(PollObjectSO);
			var service:PollService=injector.instantiate(PollService);
			injector.mapValue(IPollService,service);
			injector.mapValue(PollService,service);
			
			mediatorMap.mapView(PollToolbar, PollToolbarMediator);			
			mediatorMap.mapView(PollView, PollViewMediator);			
			mediatorMap.mapView(PollEditorView, PollEditorViewMediator);			
			mediatorMap.mapView(PollAnswerEditor, PollAnswerEditorMediator);			
			mediatorMap.mapView(PollQuestionEditor, PollQuestionEditorMediator);			
			mediatorMap.mapView(PollQuizView, PollQuizViewMediator);
			mediatorMap.mapView(PollResultsView, PollResultsViewMediator);
		}
	}
}

