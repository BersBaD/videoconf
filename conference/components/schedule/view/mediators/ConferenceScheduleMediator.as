package com.vci.conference.components.schedule.view.mediators
{
	import com.vci.components.calendar.events.CalendarEvent;
	import com.vci.components.calendar.model.ICalendarConfVO;
	import com.vci.components.calendar.view.CalendarPanel;
	import com.vci.conference.components.schedule.model.vo.CalendarConfVO;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Mediator;
	
	import ru.pwi.archive.calendar.event.CalendarSelectedDateChangedEvent;
	import ru.pwi.archive.calendar.model.Calendar;
	import ru.pwi.archive.calendar.model.CalendarPanelModel;
	import ru.pwi.archive.calendar.model.ConferencesCalendarModel;
	import ru.pwi.archive.calendar.rpc.CallbackCalendar;
	import ru.pwi.archive.calendar.view.ConferenceSchedule;
	import ru.pwi.conference.model.Conference;
	import ru.pwi.logging.Log;
	import ru.pwi.model.ModelLocator;
	
	public class ConferenceScheduleMediator extends Mediator
	{
		[Inject]
		public var viewRef:ConferenceSchedule;
		private var model:ModelLocator = ModelLocator.getInstance();
		
		private var logger:Log = Log.getLog(ConferenceScheduleMediator);
		
		private function get calendarModel():ConferencesCalendarModel{
			return viewRef.calendarModel;
		}
		private function get calPanelModel():CalendarPanelModel{
			return viewRef.calPanelModel;
		}
		
		override public function onRegister():void{
			logger.debug('ConferenceScheduleMediator#onRegister');
			viewRef.calendarViewMonth.logger=Log.getLog(CalendarPanel);
			eventMap.mapListener(viewRef.calendarViewMonth,CalendarEvent.DAY_CLICK, onDayClick);
			eventMap.mapListener(viewRef.calendarViewDay,CalendarEvent.SET_VIEW_BY_MONTH, onSetViewByMonth);
			eventMap.mapListener(viewRef.calendarViewDay,CalendarEvent.EDIT_CONFERENCE,onEditConferenceClick);
			onViewCreationComplete();
		}
		
		override public function onRemove():void{
			if(calendarModel!=null){
				removeEventListenersFromModel(calendarModel);
			}
			if (calPanelModel!=null){
				removeEventListenersFromCalModel();
			}
			eventMap.unmapListener(viewRef.calendarViewMonth,CalendarEvent.DAY_CLICK, onDayClick);
			eventMap.unmapListener(viewRef.calendarViewDay,CalendarEvent.SET_VIEW_BY_MONTH, onSetViewByMonth);
			eventMap.unmapListener(viewRef.calendarViewDay,CalendarEvent.EDIT_CONFERENCE,onEditConferenceClick);			
		}
		
		private function addEventsListenersToModel(model:ConferencesCalendarModel):void{
			calendarModel.addEventListener(CalendarSelectedDateChangedEvent.MODEL_CHANGED_EVENT, updateActivePeriod);
			//updateActivePeriod();
			calendarModel.conferences.addEventListener(CollectionEvent.COLLECTION_CHANGE, onConferencesChange);
			calendarModel.records.addEventListener(CollectionEvent.COLLECTION_CHANGE, onConferencesChange);
		}
		
		private function removeEventListenersFromModel(model:ConferencesCalendarModel):void{
			calendarModel.removeEventListener(CalendarSelectedDateChangedEvent.MODEL_CHANGED_EVENT, updateActivePeriod);
			calendarModel.conferences.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onConferencesChange);
			calendarModel.records.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onConferencesChange);
		}		

		private function addCalPanelModelListeners():void{
			calPanelModel.eventDispatcher=new EventDispatcher();
			calPanelModel.eventDispatcher.addEventListener(CalendarEvent.SELECTED_CHANGED,onSelectedChanged);
		}
		
		private function removeEventListenersFromCalModel():void{
			calPanelModel.eventDispatcher.removeEventListener(CalendarEvent.SELECTED_CHANGED,onSelectedChanged);
		}
				
		private function updateActivePeriod(ev:CalendarSelectedDateChangedEvent):void{
			logger.debug('updateActivePeriod '+calendarModel.selectedCalendarMode);
			
			switch (calendarModel.selectedCalendarMode){
				case ConferencesCalendarModel.MODE_MONTH:
					/**viewRef.calPanelModel.setSelectedDay(calendarModel.selectedDate.date);
					viewRef.calPanelModel.setSelectedMonth(calendarModel.selectedDate.month);
					viewRef.calPanelModel.setSelectedYear(calendarModel.selectedDate.fullYear);*/
					viewRef.calendarViewMonth.visible=true;
					viewRef.calendarViewMonth.includeInLayout=true;
					viewRef.calendarViewDay.visible=false;
					viewRef.calendarViewDay.includeInLayout=false;
					break;
				case ConferencesCalendarModel.MODE_DAY:
					viewRef.calendarViewMonth.visible=false;
					viewRef.calendarViewMonth.includeInLayout=false;
					viewRef.calendarViewDay.visible=true;
					viewRef.calendarViewDay.includeInLayout=true;
					break;
			}
			updateCalControls();
		}
		
		private function onConferencesChange(ev:CollectionEvent):void{
			updateCalControls();
		}

		private function onEditConferenceClick(ev:CalendarEvent):void{
			var vo:CalendarConfVO=ev.calendarConfVO as CalendarConfVO;
			if (vo!=null && vo.conf!=null){
				vo.conf.showInfo();
			}			
		}
		
		private function onViewCreationComplete():void{
			viewRef.calendarViewMonth.visible=false;
			viewRef.calendarViewDay.visible=false;
			model.confManagerModel.isPending = true;
			new CallbackCalendar(onCalendarRecieved).call();
		}
		
		private function onCalendarRecieved(calendar:Calendar):void{
			var m:ConferencesCalendarModel = new ConferencesCalendarModel();
			m.records = calendar.archive;
			m.conferences = calendar.scheduled;
			m.calendarAreaHeight = viewRef.calendarCanvas.height;
			m.calendarAreaWidth = viewRef.calendarCanvas.width;
			viewRef.calendarModel = m;
			addEventsListenersToModel(m);
			
			viewRef.calPanelModel=new CalendarPanelModel();
			calPanelModel.archive=calendar.archive;
			calPanelModel.scheduled=calendar.scheduled;

			viewRef.calendarViewMonth.model=calPanelModel;
			viewRef.monthStepper.model=calPanelModel;
			viewRef.yearsStepper.model=calPanelModel;
			viewRef.calendarViewDay.model=calPanelModel;
			
			addCalPanelModelListeners();
			onSelectedChanged(null);
			
			model.confManagerModel.isPending = false;

			viewRef.calendarViewMonth.visible=true;
			viewRef.calendarViewMonth.includeInLayout=true;
			viewRef.calendarViewDay.visible=false;
			viewRef.calendarViewDay.includeInLayout=false;
			updateCalControls();
		}		
		
		private function onDayClick(event:CalendarEvent):void{
			calendarModel.selectedDate=new Date(calPanelModel.getSelectedYear(),calPanelModel.getSelectedMonth(), event.dayNum);
			calendarModel.selectedCalendarMode=ConferencesCalendarModel.MODE_DAY;
		}

		private function onSelectedChanged(ev:CalendarEvent):void{
			updateCalControls();
		}
		
		private function onSetViewByMonth(ev:CalendarEvent):void{
			logger.debug('onSetViewByMonth');
			calendarModel.selectedCalendarMode=ConferencesCalendarModel.MODE_MONTH;
			updateCalControls();
		}
		
		private function updateCalControls():void{
			viewRef.yearsStepper.updateFromModel();
			viewRef.monthStepper.updateFromModel();

			if (viewRef.calendarViewMonth.visible){
				viewRef.calendarViewMonth.update();
			}
 
			if (viewRef.calendarViewDay.visible){
				var a:ArrayCollection=new ArrayCollection();
				var calVO:CalendarConfVO;
				for (var i:int=0;i<viewRef.calendarModel.conferences.length;i++){
					calVO=new CalendarConfVO();
					calVO.scheduled=true;
					calVO.conf=viewRef.calendarModel.conferences[i];
					a.addItem(calVO);
				}
				for (var j:int=0;j<viewRef.calendarModel.records.length;j++){
					calVO=new CalendarConfVO();
					calVO.archived=true;
					calVO.conf=viewRef.calendarModel.records[j];
					a.addItem(calVO);
				}				
				viewRef.calendarViewDay.logger=new Log('viewRef#calendarViewDay');
				
				viewRef.calendarViewDay.conferences= a;
				viewRef.calendarViewDay.update();
			}
		}
	}
}
