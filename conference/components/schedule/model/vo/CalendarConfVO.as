package com.vci.conference.components.schedule.model.vo
{
	import com.vci.components.calendar.model.ICalendarConfVO;
	
	import ru.pwi.archive.calendar.model.Conference;
	
	public class CalendarConfVO implements ICalendarConfVO
	{
		public var conf:Conference;
		
		public var scheduled:Boolean;
		public var archived:Boolean;
		
		public function get name():String
		{
			return conf.name;
		}
		
		public function get beginDate():Date
		{
			return conf.date;
		}
	}
}
