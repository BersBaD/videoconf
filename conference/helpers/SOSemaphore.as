package com.vci.conference.helpers
{
	import org.robotlegs.mvcs.Actor;
	
	public class SOSemaphore extends Actor
	{
		private var map:Object; 
		
		public function SOSemaphore():void{
			map=new Object;
		} 
		
		public function setKey(soName:String,soSlot:String):void{
			map[soName+':'+soSlot]=1;
		}
		public function clearKey(soName:String,soSlot:String):void{
			map[soName+':'+soSlot]=null;
		}
		public function getKey(soName:String,soSlot:String):Boolean{
			return (map[soName+':'+soSlot]==1)?true:false;	
		}
	}
}
