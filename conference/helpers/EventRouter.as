package com.vci.conference.helpers
{
	import com.vci.conference.components.confcontrol.events.ConferenceEvent;
	import com.vci.conference.events.AppEvent;
	
	import org.robotlegs.mvcs.Actor;
	
	import ru.pwi.helpers.misc.SimpleEvent;
	import ru.pwi.logging.Log;
	import ru.pwi.model.GUIState;
	import ru.pwi.model.ModelLocator;
	
	public class EventRouter extends Actor
	{
		private var logger:Log = Log.getLog(EventRouter);
		
		public function init():void{
			var modelLocator:ModelLocator=ModelLocator.getInstance();
			modelLocator.guiState.addWorkflowStateListener(GUIState.STATE_READY, onGUIStateChanged);
			modelLocator.guiState.addWorkflowStateListener(GUIState.STATE_AUTORIZED, onGUIStateChanged);
			modelLocator.guiState.addWorkflowStateListener(GUIState.STATE_CONNECTED, onGUIStateChanged);
			modelLocator.guiState.addWorkflowStateListener(GUIState.STATE_FAILED, onGUIStateChanged);
			modelLocator.guiState.addWorkflowStateListener(GUIState.STATE_WAIT_CONFERENCE_CLOSE, onGUIStateChanged);
			modelLocator.guiState.addWorkflowStateListener(GUIState.STATE_CONNECTING, onGUIStateChanged);

		}
		private function onGUIStateChanged(ev:SimpleEvent):void{
			dispatch(new AppEvent(AppEvent.WORKFLOW_STATE_CHANGED,ev.type));
			if (ev.type==GUIState.STATE_CONNECTED){
				dispatch(new ConferenceEvent(ConferenceEvent.START_NEW_CONFERENCE));
			}
		}
	}
}
