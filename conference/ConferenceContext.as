package com.vci.conference
{
	import com.vci.components.chat.view.ChatTabBar;
	import com.vci.conference.commands.StartupCommand;
	import com.vci.conference.commands.StartupUICommand;
	import com.vci.conference.components.broadcaster.view.mediators.BroadcasterViewMediator;
	import com.vci.conference.components.broadcaster.view.mediators.ParticipantViewMediator;
	import com.vci.conference.components.confcontrol.commands.PrivelegesChangeCommand;
	import com.vci.conference.components.confcontrol.events.PrivelegeChangeEvent;
	import com.vci.conference.components.confcontrol.model.ConferenceModel;
	import com.vci.conference.components.poll.commands.StartupPollCommand;
	import com.vci.conference.components.rightpanel.view.mediators.ChatTabBarMediator;
	import com.vci.conference.components.schedule.view.mediators.ConferenceScheduleMediator;
	import com.vci.conference.events.AppEvent;
	import com.vci.conference.helpers.EventRouter;
	import com.vci.conference.helpers.SOSemaphore;
	import com.vci.conference.model.IModel;
	import com.vci.conference.model.Model;
	import com.vci.conference.view.mediators.ConferenceMediator;
	
	import flash.display.DisplayObjectContainer;
	
	import mx.core.FlexGlobals;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;
	
	import ru.pwi.archive.calendar.view.ConferenceSchedule;
	import ru.pwi.conference.broadcasting.view.BroadcasterView;
	import ru.pwi.conference.broadcasting.view.ParticipantView;
	import ru.pwi.conference.model.Participant;
	import ru.pwi.conference.view.ViewConferenceSettings;
	import ru.pwi.logging.Log;
	import ru.pwi.view.mediators.ViewConferenceSettingsMediator;
	
	public class ConferenceContext extends Context
	{
		private var logger:Log = Log.getLog(ConferenceContext);
		public function ConferenceContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);
		//	commandMap.mapEvent(AppEvent.APP_INIT,StartupCommand,AppEvent,true);
//			dispatchEvent(new AppEvent(AppEvent.APP_INIT));
		}
		
		override public function startup():void{
			//logger.info('startup started 187.49');
			commandMap.mapEvent(AppEvent.APP_COMPLETE, StartupUICommand);
			
			/**
			 * Conference manager commands
			 */ 
			commandMap.mapEvent(PrivelegeChangeEvent.DISABLE_AUDIO,PrivelegesChangeCommand,PrivelegeChangeEvent);
			commandMap.mapEvent(PrivelegeChangeEvent.ENABLE_AUDIO,PrivelegesChangeCommand,PrivelegeChangeEvent);
			commandMap.mapEvent(PrivelegeChangeEvent.DISABLE_VIDEO,PrivelegesChangeCommand,PrivelegeChangeEvent);
			commandMap.mapEvent(PrivelegeChangeEvent.ENABLE_VIDEO,PrivelegesChangeCommand,PrivelegeChangeEvent);
			
			commandMap.mapEvent(AppEvent.APP_COMPLETE,StartupPollCommand);
			
			injector.mapSingleton(EventRouter);
			var model:Model=  injector.instantiate(Model);
			injector.mapValue(Model,model);
			injector.mapValue(IModel,model);
			
			injector.mapSingleton(ConferenceModel);
			injector.mapSingleton(SOSemaphore);
			
			mediatorMap.mapView(Conference_1,ConferenceMediator);
			mediatorMap.mapView(ChatTabBar,ChatTabBarMediator);
			mediatorMap.mapView(ConferenceSchedule,ConferenceScheduleMediator);
			mediatorMap.mapView(ParticipantView,ParticipantViewMediator);
			mediatorMap.mapView(BroadcasterView,BroadcasterViewMediator);
			
			mediatorMap.mapView(ViewConferenceSettings,ViewConferenceSettingsMediator);
			
			mediatorMap.createMediator(Conference_1(FlexGlobals.topLevelApplication));
			//logger.info('startup complete');
		}
	}
}
