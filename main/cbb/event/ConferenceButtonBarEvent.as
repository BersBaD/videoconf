package com.vci.main.cbb.event
{
	import flash.events.Event;
	
	public class ConferenceButtonBarEvent extends Event
	{
		public static const RECORD_CLICK:String = "recordClick";
		public static const LINK_CLICK:String = "linkClick";
		public static const LINKS_CLICK:String = "linksClick";
		public static const LINKS_POLL_CLICK:String = "linksPollClick";
		public static const INVITE_CLICK:String = "inviteClick";
		public static const SETTINGS_CLICK:String = "settingsClick";
		public static const OPTIONS_CLICK:String = "optionsClick";
		public static const ASK_WORD_CLICK:String = "askWordClick";
		public static const POINTER_CLICK:String = "pointerClick";
		public static const CLOSE_CONFERENCE_CLICK:String = "closeConferenceClick";
		
		public function ConferenceButtonBarEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
