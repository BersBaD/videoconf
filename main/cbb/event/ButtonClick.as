package com.vci.main.cbb.event
{
	import flash.events.Event;
	
	public class ButtonClick extends Event
	{
		public var button:String;
		
		public function ButtonClick(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}
